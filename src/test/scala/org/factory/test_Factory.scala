package org.factory

import org.mavok.api.table.TableModel
import org.scalatest.{FlatSpec, Matchers}
import org.testModels.{TableModelRecord, TableModelTest}
import testEngine.TableBuilder

class test_Factory extends FlatSpec with Matchers with TableBuilder{

  override val tableList: Vector[TableModel[_ <: Product]] = Vector( TableModelTest )

  "Factory" should "create table model with PKs" in {
    val nextRecord = TableModelRecord( 1, "test", "test" )
    require( TableModelTest.controller.count() == 0, s"Count: ${TableModelTest.controller.count()}" )
    TableModelTest.controller += nextRecord
    require( TableModelTest.controller.count() == 1, s"Count: ${TableModelTest.controller.count()}" )
  }


  it should "create table with PKs, which factory can read in correctly" in {
    val nextRecord = TableModelRecord( 2, "test", "test" )
    TableModelTest.controller += nextRecord
    val results = TableModelTest.controller.filter( _.colPkId :== "2" )
    require( results.size == 1 )
    require( results.head == nextRecord )
  }

  it should "update 1 value in table" in {

    val nextRecord = TableModelRecord( 2, "test", "test" )
    TableModelTest.controller.drop
    TableModelTest.controller.create
    TableModelTest.controller += nextRecord
    val updatedRecord = TableModelRecord( 2, "test", "test5" )
    TableModelTest.controller.update( updatedRecord )

    val results = TableModelTest.controller.filter( _.colPkId :== "2" )
    require( results.size == 1 )
    require( results.head == updatedRecord )
  }


  it should "delete 1 value in table" in {

    TableModelTest.controller.drop
    TableModelTest.controller.create

    val nextRecord = TableModelRecord( 2, "test", "test" )
    TableModelTest.controller += nextRecord
    TableModelTest.controller.delete( nextRecord )

    val results = TableModelTest.controller.filter( _.colPkId :== "2" )
    require( results.isEmpty )
  }

}
