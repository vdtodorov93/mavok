package org.sqlGenerator.sqlStatements

import org.mavok.common.ConnectionType
import org.sqlGenerator.JDBCConnection

trait SqlTestCases {
  def connectionType: ConnectionType.Value
  def jdbcConnection: JDBCConnection
  def dbName: String

  def SimpleSelectActor: String
  def SelectFemaleActors: String
  def TwoInnerJoins: String
  def TwoLeftJoins: String
  def TwoRightJoins: String
  def SelectMoviesWithFemaleActors: String
  def ImportActorsFromPersonTable: String
  def UpdateMovieName: String
  def UpdateCharactersNameForMovie: String
  def groupByGenderActorsOlderThan: String

  def createTableMovie: String
  def createTableStarsIn: String
  def createTableActor: String
  def createTablePerson: String

  def dropTables: String
  def insertData: String
}

object SqlTestCases {
  val ValidCases: List[SqlTestCases] = PostgresSQLStatements  :: Nil //TODO: Add OracleSQLStatements
}
