package org.sqlGenerator.sqlStatements

import org.mavok.common.ConnectionType

object OracleSQLStatements extends SqlTestCases {
  override val connectionType: ConnectionType.Value = ConnectionType.ORACLE

  override val jdbcConnection = ???

  override val dbName = "Oracle"

  override val SimpleSelectActor: String = "TODO"

  override val SelectFemaleActors: String = "TODO"

  override val TwoInnerJoins: String = "TODO"

  override val TwoLeftJoins: String = "TODO"

  override val TwoRightJoins: String = "TODO"

  override val SelectMoviesWithFemaleActors: String = "TODO"

  override val ImportActorsFromPersonTable: String = "TODO"

  override val UpdateMovieName: String = "TODO"

  override val UpdateCharactersNameForMovie: String = "TODO"

  override val groupByGenderActorsOlderThan = "TODO"

  override val createTableMovie: String = "TODO"

  override val  createTableStarsIn: String = "TODO"

  override val  createTableActor: String = "TODO"

  override val  createTablePerson: String = "TODO"

  override val dropTables: String = "TODO"

  override val insertData: String = "TODO"
}
