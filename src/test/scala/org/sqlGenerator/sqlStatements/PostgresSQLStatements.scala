package org.sqlGenerator.sqlStatements

import org.common.Globals
import org.mavok.common.ConnectionType
import org.sqlGenerator.JDBCConnection

object PostgresSQLStatements extends SqlTestCases {

  override val connectionType = ConnectionType.POSTGRES

  override val jdbcConnection = Globals.TEST_POSTGRES_CONNECTION

  override val dbName = "Postgres"

  override val SimpleSelectActor =
    """
      |SELECT actr.name actn, actr.birth_date acbd
      |FROM scala_testing.actor actr
    """.stripMargin

  override val SelectFemaleActors =
    """
      |SELECT actr.name actn FROM scala_testing.actor actr
      |WHERE actr.gender = 'F'
      |AND actr.age > 32
    """.stripMargin

  override val TwoInnerJoins =
    """
      |SELECT actr.name acnm, mvie.name mvnm, stri.character_name chnm FROM scala_testing.actor actr
      |INNER JOIN scala_testing.stars_in stri
      |ON ( actr.id ) = ( stri.actor_id )
      |INNER JOIN scala_testing.movie mvie
      |ON ( stri.movie_id ) = ( mvie.id )
    """.stripMargin

  override val TwoLeftJoins =
    """
      |SELECT actr.name acnm, mvie.name mvnm, stri.character_name chnm FROM scala_testing.actor actr
      |LEFT OUTER JOIN scala_testing.stars_in stri
      |ON ( actr.id ) = ( stri.actor_id )
      |LEFT OUTER JOIN scala_testing.movie mvie
      |ON ( stri.movie_id ) = ( mvie.id )
    """.stripMargin

  override val TwoRightJoins =
    """
      |SELECT actr.name acnm, mvie.name mvnm, stri.character_name chnm FROM scala_testing.actor actr
      |RIGHT OUTER JOIN scala_testing.stars_in stri
      |ON ( actr.id ) = ( stri.actor_id )
      |RIGHT OUTER JOIN scala_testing.movie mvie
      |ON ( stri.movie_id ) = ( mvie.id )
    """.stripMargin

  override val SelectMoviesWithFemaleActors =
    """
      |SELECT mov1.name FROM scala_testing.movie mov1
      |WHERE mov1.id IN (
      |    SELECT mov2.id FROM scala_testing.movie mov2
      |    INNER JOIN scala_testing.stars_in stri
      |    ON ( mov2.id ) = ( stri.movie_id )
      |    INNER JOIN scala_testing.actor actr
      |    ON ( stri.actor_id ) = ( actr.id )
      |    WHERE actr.gender = 'F'
      |)
    """.stripMargin

  override val ImportActorsFromPersonTable =
    """
      |INSERT INTO scala_testing.actor(name, gender, age)
      |SELECT prsn.full_name pfnm, prsn.gender pgnd, prsn.age page
      |FROM scala_testing.person prsn
      |WHERE ( prsn.profession ) = ( 'Actor' )
    """.stripMargin

  override val UpdateMovieName =
    """
      |UPDATE scala_testing.movie
      |SET name = 'Star Wars'
      |WHERE name = 'Star Wars III'
    """.stripMargin

  override val UpdateCharactersNameForMovie =
    """
      |UPDATE scala_testing.stars_in stri
      |SET character_name = 'Evil Star Wars Participator'
      |WHERE (stri.actor_id, stri.movie_id) IN (
      |    SELECT actr.id, mvie.id
      |    FROM scala_testing.actor actr
      |             INNER JOIN scala_testing.stars_in sin2
      |                        ON ( actr.id ) = ( sin2.actor_id )
      |             INNER JOIN scala_testing.movie mvie
      |                        ON ( sin2.movie_id ) = ( mvie.id )
      |    WHERE ( mvie.name ) = ( 'Star Wars III' )
      |)
    """.stripMargin

  override val groupByGenderActorsOlderThan =
    """
      |SELECT ac.gender as gender, COUNT(ac.age) as count from scala_testing.actor ac
      |WHERE ac.age > 32
      |GROUP BY ac.gender;
    """.stripMargin

  override val createTableMovie: String =
    """create table scala_testing.movie
      |(
      |	id serial constraint movie_pk primary key,
      |	author varchar(255),
      |	name varchar(255) not null,
      |	imdb_rating double precision,
      |	year_produced int
      |)""".stripMargin

  override val  createTableStarsIn: String =
    """create table scala_testing.stars_in
      |(
      |    movie_id integer not null constraint movie_fk references scala_testing.movie,
      |    actor_id integer not null constraint actor_id references scala_testing.actor,
      |    character_name varchar(255)
      |)""".stripMargin

  override val  createTableActor: String =
    """create table scala_testing.actor
      |(
      |	id serial constraint actor_pk primary key,
      |	name varchar(255) not null,
      |	gender char(1),
      |	birth_date timestamp,
      |  age int
      |)""".stripMargin

  override val  createTablePerson: String =
    """create table scala_testing.person
      |(
      |	id serial constraint person_pk primary key,
      |	full_name varchar(255) not null,
      |	age int not null,
      |	profession varchar(255),
      |	gender char(1) not null
      |)""".stripMargin

  override val dropTables: String =
    """
      |drop table if exists scala_testing.person CASCADE;
      |drop table if exists scala_testing.stars_in CASCADE;
      |drop table if exists scala_testing.movie CASCADE;
      |drop table if exists scala_testing.actor CASCADE;
    """.stripMargin

  override val insertData: String =
    """
      |INSERT INTO scala_testing.movie(name, author, imdb_rating, year_produced)
      |VALUES
      |    ('Movie no one acted in', 'No one', 0.1, 2025),
      |    ('Star Wars III', 'George lucaC', 9.2, 2004),
      |    ('Fast and Furious', 'Gary Scott Thompson', 8.3, 2001),
      |    ('American Pie', 'Chris Moore', 7.4, 1998);
      |
      |INSERT INTO scala_testing.actor(name, gender, birth_date, age)
      |VALUES
      |('Ken Block', 'M', NOW() - interval '31 year', 31),
      |    ('Pamela Anderson', 'F', NOW() - interval '44 year', 44),
      |    ('Rihanna', 'F', NOW() - interval '34 year', 34),
      |    ('Vin Diesel', 'M', NOW() - interval '52 year', 52),
      |    ('Paul Walker', 'M', NOW() - interval '48 year', 48),
      |    ('Natasha Lyonne', 'F', NOW() - interval '45 year', 45),
      |    ('Chris Owen', 'M', NOW() - interval '67 year', 67),
      |    ('Tara Reid', 'F', NOW() - interval '44 year', 44),
      |    ('Ewan McGregor', 'M', NOW() - interval '74 year', 74),
      |    ('Natalie Portman', 'F', NOW() - interval '56 year', 56);
      |
      |    INSERT INTO scala_testing.stars_in(movie_id, character_name, actor_id)
      |VALUES
      |    (1, 'Obi-Wan Kenobi', 6),
      |    (1, 'Padme', 7),
      |    (2, 'Dominic Toretto', 1),
      |    (2, 'Brian Oconor', 2),
      |    (3, 'Jessica', 3),
      |    (3, 'Chuck Sherman', 4),
      |    (3, 'Victoria', 5),
      |    (3, 'Vin Diesel didnt play here', 1);
      |
      |INSERT INTO scala_testing.person (id, full_name, age, profession, gender)
      | VALUES
      |(1, 'Ivan Ivanov', 24, 'Pilot', 'M'),
      |(2, 'Janine', 28, 'Project Manage', 'F'),
      |(3, 'Helen McCrory', 56, 'Actor', 'F'),
      |(4, 'Cillian Murphy', 47, 'Actor', 'M');
    """.stripMargin
}
