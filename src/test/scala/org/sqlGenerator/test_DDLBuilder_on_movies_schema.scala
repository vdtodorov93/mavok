package org.sqlGenerator

import org.mavok.LOG
import org.mavok.api.sql.DDLBuilder
import org.mavok.api.sql.DMLBuilder
import org.mavok.common.ConnectionType
import org.scalatest.{FlatSpec, Matchers}
import org.sqlGenerator.sqlStatements.SqlTestCases
import org.testModels.ActorTable
import org.testModels.MovieTable
import org.testModels.PersonTable
import org.testModels.StarsInTable


class test_DDLBuilder_on_movies_schema extends FlatSpec with Matchers {

  for (testCase <- SqlTestCases.ValidCases) {
    "DDL Engine " should s"generate SQL for create table movie with DB ${testCase.dbName}" in {
      val sqlStatement = DDLBuilder(MovieTable).create.build(testCase.connectionType)
      SqlGeneratorTestUtilities.compareSql(generated = sqlStatement, expected = testCase.createTableMovie)
    }

    "DDL Engine " should s"generate SQL for create table with foreign keys with DB ${testCase.dbName}" in {
      val sqlStatement = DDLBuilder(StarsInTable).create.build(testCase.connectionType)
      SqlGeneratorTestUtilities.compareSql(generated = sqlStatement, expected = testCase.createTableStarsIn)
    }

    "DDL Engine " should s"generate SQL for create table actor with DB ${testCase.dbName}" in {
      val sqlStatement = DDLBuilder(ActorTable).create.build(testCase.connectionType)
      SqlGeneratorTestUtilities.compareSql(generated = sqlStatement, expected = testCase.createTableActor)
    }

    //fails because .asPk and .asNonNull do not work
    "DDL Engine " should s"generate SQL for create table person with DB ${testCase.dbName}" in {
      val sqlStatement = DDLBuilder(PersonTable).create.build(testCase.connectionType)
      SqlGeneratorTestUtilities.compareSql(generated = sqlStatement, expected = testCase.createTablePerson)
    }
  }
}
