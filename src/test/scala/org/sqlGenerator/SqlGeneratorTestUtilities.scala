package org.sqlGenerator

import org.scalatest.Matchers

import scala.collection.mutable

object SqlGeneratorTestUtilities extends Matchers {
  def compareSql(generated: String, expected: String) = {
    println("GENERATED NORMALIZED")
    println(normalizeSql(generated))
    println("EXPECTED NORMALIZED")
    println(normalizeSql(expected))
    assert(normalizeSql(generated) == normalizeSql(expected))
  }

  def compareAliasedSql(generated: String, expected: String) = {
    println("GENERATED NORMALIZED")
    println(normalizeSql(generated))
    println("EXPECTED NORMALIZED")
    println(normalizeSql(expected))
    assert(comparedAliasesSql(mavokSql = normalizeSql(generated), expectedSql = normalizeSql(expected)))
  }



  def main(args: Array[String]): Unit = {
//    val mySql =
//      """
//        |SELECT actr.name acnm, actr.birth_date bday
//        |FROM scala_testing.actor actrtr
//      """.stripMargin
//    val mavokSql = "select zzz1.name zzz2, zzz1.birth_date zzz3 from scala_testing.actor zzz1"

  }

  private[this] def comparedAliasesSql(mavokSql: String, expectedSql: String): Boolean = {

    val pattern = "zzz([0-9]*)".r
    val matches = pattern.findAllMatchIn(mavokSql)
    val positions = matches.map(m => m.start -> m.end).toList

    val mavokAliasesWithPositions = positions.map {
      case (begin, end) => mavokSql.substring(begin, end) -> begin
    }.toList
    val mavokPositions = mavokAliasesWithPositions.groupBy(_._1).map(_._2.map(_._2))
    val mavokAliases = mavokAliasesWithPositions.map(_._1)

    var offset = 0
    val expectedPositionsList = new mutable.ArrayBuffer[(Int, Int)]()
    positions.foreach(positions => {
      expectedPositionsList.append(positions._1 - offset -> (positions._1 - offset + 4))
      offset = offset + (positions._2 - positions._1 - 4)
    })

    val expectedAliasesWithPositions = expectedPositionsList.map {
      case (begin, end) => expectedSql.substring(begin, begin + 4) -> begin
    }.toList
    val expectedAliases = expectedAliasesWithPositions.map(_._1)

    val mavokPositionsFlattened = mavokAliasesWithPositions.groupBy(_._1).toList.sortBy(_._2.head._2).zipWithIndex.flatMap {
      case (aliasWithPos, index) => aliasWithPos._2.map(_._2 -> index)
    }.sortBy(_._1).map(_._2)

    val expectedPositionsFlattened =  expectedAliasesWithPositions.groupBy(_._1).toList.sortBy(_._2.head._2).zipWithIndex.flatMap {
      case (aliasWithPos, index) => aliasWithPos._2.map(_._2 -> index)
    }.sortBy(_._1).map(_._2)

    mavokPositionsFlattened == expectedPositionsFlattened && removeAliases(mavokSql, mavokAliases) == removeAliases(expectedSql, expectedAliases)
  }

  private[this] def removeAliases(str: String, aliases: List[String]): String = {
    var result = str
    aliases.foreach(alias => result = result.replaceAll(alias, ""))
    result
  }

  private[this] def normalizeSql(sql: String): String = {
    sql.toLowerCase
      .replaceAll("[\\s\\t\\n]+", " ").trim
      .replaceAll("\\s*,\\s*", ", ")
      .replaceAll("\\s*\\(\\s*", " ( ")
      .replaceAll("\\s*\\)\\s*", " ) ")
  }

}
