package org.sqlGenerator

import org.common.Globals
import org.mavok.LOG
import org.mavok.api.column.ColumnFactory
import org.mavok.api.sql.DMLBuilder
import org.mavok.datatype.dbtype.{BIGINT, DBColumnType}
import org.scalatest.{FlatSpec, Matchers}
import org.testModels.{Table2Columns, Table3Columns, TableModelTest}

class test_TableQueries  extends FlatSpec with Matchers {


  "DML Engine" should "generate sql for left outer join" in {

    val sqlStatement = DMLBuilder( Table2Columns )
      .select( Table2Columns.colInt,  Table2Columns.colString )
      .left( Table3Columns )
      .on( ( Table3Columns.colString :== Table2Columns.colString )
        && ( Table3Columns.colString :== Table2Columns.colString )
        && ( Table3Columns.colInt :== Table2Columns.colInt )
        && ( Table3Columns.colString :== Table2Columns.colString )
      )
      .groupBy( Table2Columns.colInt, Table2Columns.colString )
      .orderBy( Table2Columns.colInt, Table2Columns.colString )
      .where( Table3Columns.colString isNull )
      .build( Globals.TEST_POSTGRES_CONNECTION.getConnectionType )

    LOG.info( "Finished test build", sqlStatement)

  }


  it should "generate sql for inner join" in {

    val sqlStatement = DMLBuilder( Table2Columns )
      .select( Table2Columns.colInt,  Table2Columns.colString.as[BIGINT]( DBColumnType.BIGINT ) )
      .inner( Table3Columns )
      .on( ( Table3Columns.colString :== Table2Columns.colString )
        && ( Table3Columns.colString :== Table2Columns.colString )
        && ( Table3Columns.colInt :== Table2Columns.colInt )
        && ( Table3Columns.colString :== Table2Columns.colString )
      )
      .where( Table3Columns.colString isNull )
      .build( Globals.TEST_POSTGRES_CONNECTION.getConnectionType )

    LOG.info( "Finished test build", sqlStatement)
  }


  it should "generate sql for inner join with anonymous classes" in {

    import org.mavok.api.Implicits._
    val sqlStatement = DMLBuilder( Table2Columns )
      .select( "1", "2" , Table2Columns.colInt,  Table2Columns.colString )
      .inner( Table3Columns )
      .on( ( Table3Columns.colString isLike Table2Columns.colString )
      )
      .left( TableModelTest )
      .on( ( Table3Columns.colString :== TableModelTest.colApple )
        && ( Table3Columns.colString :== TableModelTest.colOrange )
        && ( Table3Columns.colInt :== TableModelTest.colPkId )
      )
      .where( Table3Columns.colString isNull )
      .build( Globals.TEST_POSTGRES_CONNECTION.getConnectionType )

    LOG.info( "Finished test build", sqlStatement)
  }


  it should "generate sql for inner join with getColumns" in {
    val sqlStatement = DMLBuilder( Table2Columns )
      .select( Table2Columns.getColumns: _* )
      .left( Table3Columns )
      .on( ( Table3Columns.colString :== Table2Columns.colInt )
        && ( Table3Columns.colString :== Table2Columns.colString )
        && ( Table3Columns.colInt :== Table2Columns.colInt )
      )
      .where( Table3Columns.getColumns.filter( ! _.isPK ).head isNull )
      .insert( Table3Columns )( Table3Columns.getColumns: _* )
      .build( Globals.TEST_POSTGRES_CONNECTION.getConnectionType )

    LOG.info( "Finished test build", sqlStatement)
  }

  it should "generate sql for coalesce with defaults" in {

    val sqlStatement = DMLBuilder( Table2Columns )
      .select( Table2Columns.colInt,  Table2Columns.colString.as[BIGINT]( DBColumnType.BIGINT ) )
      .inner( Table3Columns )
      .on( ( Table3Columns.colString :== Table2Columns.colString
        .getOrElse( ColumnFactory.buildDefaultValueForType(
          Table2Columns.colString.dbType, Globals.TEST_POSTGRES_CONNECTION )) )
        && ( Table3Columns.colString :== Table2Columns.colString )
        && ( Table3Columns.colInt :== Table2Columns.colInt )
        && ( Table3Columns.colString :== Table2Columns.colString )
      )
      .where( Table3Columns.colString isNull )
      .build( Globals.TEST_POSTGRES_CONNECTION.getConnectionType )

    LOG.info( "Finished test build", sqlStatement)
  }




}
