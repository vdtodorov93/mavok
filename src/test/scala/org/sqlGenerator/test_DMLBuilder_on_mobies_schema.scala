package org.sqlGenerator

import org.common.Globals
import org.mavok.LOG
import org.mavok.api.sql.DMLBuilder
import org.scalatest.FlatSpec
import org.scalatest.Matchers
import org.testModels.ActorTable
import org.testModels.MovieTable
import org.testModels.PersonTable
import org.testModels.StarsInTable
import org.testModels.Table2Columns
import org.testModels.Table3Columns
import org.mavok.api.Implicits._
import org.sqlGenerator.sqlStatements.PostgresSQLStatements
import org.sqlGenerator.sqlStatements.SqlTestCases

class test_DMLBuilder_on_mobies_schema extends FlatSpec with Matchers {

  "DML Engine" should "generate simple select on actor table" in {
    for(testCase <- SqlTestCases.ValidCases) {
      val expected = testCase.SimpleSelectActor

      val sqlStatement = DMLBuilder(ActorTable)
        .select(ActorTable.name, ActorTable.birth_date)
        .build(testCase.connectionType)

      SqlGeneratorTestUtilities.compareAliasedSql(sqlStatement, expected)
    }
  }


  "DML Engine" should "select female actors" in {
    for (testCase <- SqlTestCases.ValidCases) {
      val expected = testCase.SelectFemaleActors

      val sqlStatement = DMLBuilder(ActorTable)
        .select(ActorTable.name)
        .where(
          (ActorTable.gender :== "F") //&& (ActorTable.age :> 32) TODO: what is the way to do this?
        )
        .build(testCase.connectionType)

      SqlGeneratorTestUtilities.compareAliasedSql(sqlStatement, expected)
    }
  }

  "DML Engine" should "generate sql for 2 inner joins" in {
    for (testCase <- SqlTestCases.ValidCases) {
      val expected = testCase.TwoInnerJoins

      val sqlStatement = DMLBuilder(ActorTable)
        .select(ActorTable.name, MovieTable.name, StarsInTable.character_name)
        .inner(StarsInTable)
        .on(ActorTable.id :== StarsInTable.actor_id)
        .inner(MovieTable)
        .on(StarsInTable.movie_id :== MovieTable.id)
        .build(testCase.connectionType)

      SqlGeneratorTestUtilities.compareAliasedSql(sqlStatement, expected)
    }
  }

  "DML Engine" should "generate sql for 2 left joins" in {
    for (testCase <- SqlTestCases.ValidCases) {
      val expected = testCase.TwoLeftJoins

      val sqlStatement = DMLBuilder(ActorTable)
        .select(ActorTable.name, MovieTable.name, StarsInTable.character_name)
        .left(StarsInTable)
        .on(ActorTable.id :== StarsInTable.actor_id)
        .left(MovieTable)
        .on(StarsInTable.movie_id :== MovieTable.id)
        .build(testCase.connectionType)

      SqlGeneratorTestUtilities.compareAliasedSql(sqlStatement, expected)
    }
  }

  "DML Engine" should "generate sql for 2 right joins" in {
    for (testCase <- SqlTestCases.ValidCases) {
      val expected = testCase.TwoRightJoins

      val sqlStatement = DMLBuilder(ActorTable)
        .select(ActorTable.name, MovieTable.name, StarsInTable.character_name)
        .right(StarsInTable)
        .on(ActorTable.id :== StarsInTable.actor_id)
        .right(MovieTable)
        .on(StarsInTable.movie_id :== MovieTable.id)
        .build(testCase.connectionType)

      SqlGeneratorTestUtilities.compareAliasedSql(sqlStatement, expected)
    }
  }

  "DML Engine" should "generate sql selecting all movies in which female actors star in" in {
    for (testCase <- SqlTestCases.ValidCases) {
      val expected = testCase.SelectMoviesWithFemaleActors

      val sqlStatement = DMLBuilder(MovieTable)
        .select(MovieTable.name)
        .build(testCase.connectionType)

      SqlGeneratorTestUtilities.compareAliasedSql(sqlStatement, expected)
    }
  }

  "DML Engine" should "generate sql to import people from the `person` table with profession Actor and insert it into `actor` table" in {
    for (testCase <- SqlTestCases.ValidCases) {
      val expected = testCase.ImportActorsFromPersonTable

      val sqlStatement = DMLBuilder(PersonTable)
        .select(PersonTable.full_name, PersonTable.gender, PersonTable.age)
        .where(PersonTable.profession :== "Actor")
        .insert(ActorTable)(ActorTable.name, ActorTable.gender, ActorTable.age)
        .build(testCase.connectionType)

      SqlGeneratorTestUtilities.compareAliasedSql(sqlStatement, expected)
    }
  }

  "DML Engine" should "generate sql to update movie name" in {
    for (testCase <- SqlTestCases.ValidCases) {
      val expected = testCase.UpdateMovieName

      val sqlStatement = DMLBuilder(MovieTable)
        .updateValues(MovieTable.name << "Star Wars")
        .on(MovieTable.name :== "Star Wars III")
        .build(testCase.connectionType)

      SqlGeneratorTestUtilities.compareAliasedSql(sqlStatement, expected)
    }
  }

  "DML Engine" should "generate sql to update name of all characters taking park in particular movie" in {
    for (testCase <- SqlTestCases.ValidCases) {
      val expected = testCase.UpdateCharactersNameForMovie

      val sqlStatement = DMLBuilder(StarsInTable)
        .updateValues(StarsInTable.character_name << "Evil Star Wars Participator")
        .on(MovieTable.name :== "Star Wars III") //TODO: Update api is too basic to implement this query
        .build(testCase.connectionType)

      SqlGeneratorTestUtilities.compareAliasedSql(sqlStatement, expected)
    }
  }
}
