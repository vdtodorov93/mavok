package org.sqlGenerator

import org.mavok.LOG
import org.scalatest.FlatSpec
import org.scalatest.Matchers
import org.sqlGenerator.sqlStatements.SqlTestCases

import scala.collection.mutable


case class ActorResult(actn: String, acbd: String)
case class ActorFemaleResult(actn: String)
case class JoinQueryResult(acnm: String, mvnm: String, chnm: String)
case class GroupByStatementResult(gender: String, count: Int)

class JDBCPluginTest extends FlatSpec with Matchers {

  for(testCase <- SqlTestCases.ValidCases) {
    prepareTestData(testCase)


    s"JDBCPlugin execquery for ${testCase.dbName}" should "work with SELECT" in {
      val actors = new mutable.ArrayBuffer[ActorResult]()

      testCase.jdbcConnection.execQuery[ActorResult](testCase.SimpleSelectActor, s => actors.append(s))

      assert(actors.length == 10)
      assert(actors.exists(a => a.actn == "Vin Diesel"))
      assert(actors.exists(a => a.actn == "Natasha Lyonne"))
    }

    s"JDBCPlugin execquery for ${testCase.dbName}" should "work with SELECT female actor" in {
      val actors = new mutable.ArrayBuffer[ActorFemaleResult]()

      testCase.jdbcConnection.execQuery[ActorFemaleResult](testCase.SelectFemaleActors, s => actors.append(s))

      assert(actors.length == 5)
      assert(actors.forall(a => a.actn != "Vin Diesel"))
      assert(actors.exists(a => a.actn == "Natasha Lyonne"))
    }

     s"JDBCPlugin execquery for ${testCase.dbName}" should "work with Two Inner Joins" in {
       val joinResults = new mutable.ArrayBuffer[JoinQueryResult]()

       testCase.jdbcConnection.execQuery[JoinQueryResult](testCase.TwoInnerJoins, s => joinResults.append(s))

       assert(joinResults.length == 8)
       assert(joinResults.contains(JoinQueryResult("Vin Diesel", "Fast and Furious", "Chuck Sherman")))
       assert(joinResults.forall(c => c.acnm != null && c.acnm != ""))
     }

     s"JDBCPlugin execquery for ${testCase.dbName}" should "work with Two Left Joins" in {
       val joinResults = new mutable.ArrayBuffer[JoinQueryResult]()

       testCase.jdbcConnection.execQuery[JoinQueryResult](testCase.TwoLeftJoins, s => joinResults.append(s))

       assert(joinResults.length == 11)
       assert(joinResults.contains(JoinQueryResult("Vin Diesel", "Fast and Furious", "Chuck Sherman")))
       assert(joinResults.exists(c => c.mvnm == null || c.mvnm == ""))
       assert(joinResults.forall(c => c.acnm != null && c.acnm != ""))
     }

     s"JDBCPlugin execquery for ${testCase.dbName}" should "work with Two Right Joins" in {
       val joinResults = new mutable.ArrayBuffer[JoinQueryResult]()

       testCase.jdbcConnection.execQuery[JoinQueryResult](testCase.TwoRightJoins, s => joinResults.append(s))

       assert(joinResults.length == 9)
       assert(joinResults.contains(JoinQueryResult("Vin Diesel", "Fast and Furious", "Chuck Sherman")))
       assert(joinResults.exists(c => c.acnm == null || c.acnm == ""))
       assert(joinResults.forall(c => c.mvnm != null && c.mvnm != ""))
     }

     s"JDBCPlugin execquery for ${testCase.dbName}" should "work with groupBy statements results" in {
       val groupByResults = new mutable.ArrayBuffer[GroupByStatementResult]()

       testCase.jdbcConnection.execQuery[GroupByStatementResult](testCase.groupByGenderActorsOlderThan, s => groupByResults.append(s))

       assert(groupByResults.length == 2)
       assert(groupByResults.contains(GroupByStatementResult("M", 4)))
       assert(groupByResults.contains(GroupByStatementResult("F", 5)))
     }

    //TODO: add test for parsing from TimeStamp, currently works only with String...
  }



  def prepareTestData(testCases: SqlTestCases) = {
    LOG.info("Preparing test data", "")
    testCases.jdbcConnection.execStatement(testCases.dropTables)
    LOG.info("Dropped tables", "")
    testCases.jdbcConnection.execStatement(testCases.createTablePerson)
    testCases.jdbcConnection.execStatement(testCases.createTableActor)
    testCases.jdbcConnection.execStatement(testCases.createTableMovie)
    testCases.jdbcConnection.execStatement(testCases.createTableStarsIn)
    LOG.info("Created tables", "")
    testCases.jdbcConnection.execStatement(testCases.insertData)
    LOG.info("Prepared test data", "")
  }
}
