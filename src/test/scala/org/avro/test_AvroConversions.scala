package org.avro

/*
 * Copyright: blacktest -- created 18.08.19
 * Documentation: https://devhat.atlassian.net/wiki/spaces/ADM/overview
 */


import java.util.UUID

import org.common.Globals
import org.mavok.LOG
import org.mavok.api.table.{TableGenericFactory, TablePath}
import org.mavok.common.classvalue.StringAvroDefinition
import org.mavok.datatype.Avro2DBColumnMapping
import org.mavok.datatype.dbtype.{DBTypeFloat, DBTypeInteger, DBTypeString}
import org.scalatest.{FlatSpec, Matchers}

import scala.io.Source


class test_AvroConversions extends FlatSpec with Matchers {


  // ---------------------  Mocking Objects


  "Avro Converter" should "convert avro definition string to Table Object" in {


    val jsonFile = Source.fromResource("jsonFiles/StringAvroTableDefinition.json")

    val avroString = jsonFile.getLines().mkString
    val avroSchema = Avro2DBColumnMapping( StringAvroDefinition( avroString ) )
    val tablePath = TablePath( avroSchema.name.name, UUID.fromString( avroSchema.schema.getNamespace()) )
    val dbList = TableGenericFactory.fromAvro( avroSchema, tablePath, Globals.TEST_POSTGRES_CONNECTION  )

    LOG.debug( "Logging table", dbList.tablePath.getTablePath )

    assert( dbList.getColumns.length == 3 )
    assert( dbList.getColumns(0).dbType.typePattern.isInstanceOf[DBTypeFloat] )
    assert( dbList.getColumns(1).dbType.typePattern.isInstanceOf[DBTypeString] )
    assert( dbList.getColumns(2).dbType.typePattern.isInstanceOf[DBTypeInteger] )
  }



  it should "should convert Avro Type to the correct DBType" in {

    val avroString = Source.fromResource("jsonFiles/StringAvroTableDefinition.json").getLines().mkString
    val avroSchema = Avro2DBColumnMapping( StringAvroDefinition( avroString ) )
    val tablePath = TablePath( avroSchema.name.name, UUID.fromString( avroSchema.schema.getNamespace()) )
    val table = TableGenericFactory.fromAvro( avroSchema, tablePath, Globals.TEST_POSTGRES_CONNECTION  )

    LOG.debug( "Logging table", table.tablePath.getTablePath )

    assert( table.getColumns.length == 3 )
    assert( table.getColumns(0).dbType.typePattern.isInstanceOf[DBTypeFloat] )
    assert( table.getColumns(1).dbType.typePattern.isInstanceOf[DBTypeString] )
    assert( table.getColumns(2).dbType.typePattern.isInstanceOf[DBTypeInteger] )

  }


  it should "throw NullPointerException if null is provided as input to any method" in {

    intercept[IllegalArgumentException]{
      TableGenericFactory.fromAvro( null, null, null  )
    }



  }

  it should "convert Table to Avro Schema Definition String correctly" in {

    val avroString = Source.fromResource("jsonFiles/StringAvroTableDefinition.json").getLines().mkString
    val avroSchema = Avro2DBColumnMapping(StringAvroDefinition(avroString))
    val tablePath = TablePath( avroSchema.name.name, UUID.fromString( avroSchema.schema.getNamespace()) )
    val table = TableGenericFactory.fromAvro(avroSchema, tablePath, Globals.TEST_POSTGRES_CONNECTION )

    val avroStringResult = Avro2DBColumnMapping.fromTable(table)

    assert(avroString.replaceAll( " ", "") == avroStringResult.schema.toString.replaceAll( " ", "") )
  }






}
