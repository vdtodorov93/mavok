package org.testModels

import org.common.Globals
import org.mavok.api.column.Column
import org.mavok.api.table.{AbstractTable, HasTablePath, TablePath}
import org.mavok.datatype.dbtype.STRING

object Table3Columns extends AbstractTable{

  val colString: Column[STRING, _ <: Table3Columns.this.type] = column[STRING]( "orange" )
  val colInt = column[STRING]( "apple" )
  val colTestMissing = column[STRING]( "missing" )

  override val tablePath: HasTablePath = TablePath( Globals.TEST_SCHEMA, "orange")
}
