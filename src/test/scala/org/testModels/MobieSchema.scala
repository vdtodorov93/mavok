package org.testModels

import org.common.Globals
import org.mavok.api.table.TableModel
import org.mavok.api.table.TablePath
import org.mavok.datatype.dbtype._

object MovieTable extends TableModel(TablePath(Globals.TEST_SCHEMA, "movie"), Globals.TEST_POSTGRES_CONNECTION) {
  val id = column[INTEGER]("id").asPK
  val author = column[STRING]("author")
  val name = column[STRING]("name").asNotNull
  val imdb_rating = column[FLOAT]("imdb_rating")
  val year_produced = column[INTEGER]("year_produced")
}

object ActorTable extends TableModel(TablePath(Globals.TEST_SCHEMA, "actor"), Globals.TEST_POSTGRES_CONNECTION) {
  val id = column[INTEGER]("id").asPK
  val name = column[STRING]("name")
  val gender = column[STRING]("gender")
  val birth_date = column[TIMESTAMP]("birth_date")
  val age = column[INTEGER]("age")
}

object StarsInTable extends TableModel(TablePath(Globals.TEST_SCHEMA, "stars_in"), Globals.TEST_POSTGRES_CONNECTION) {
  val movie_id = column[INTEGER]("movie_id").asNotNull
  val actor_id = column[INTEGER]("actor_id").asNotNull
  val character_name = column[STRING]("character_name")
}

object PersonTable extends TableModel(TablePath(Globals.TEST_SCHEMA, "person"), Globals.TEST_POSTGRES_CONNECTION) {
  val id = column[INTEGER]("id").asPK
  val full_name = column[STRING]("full_name")
  val gender = column[STRING]("gender")
  val profession = column[STRING]("profession")
  val age = column[INTEGER]("age")
}
