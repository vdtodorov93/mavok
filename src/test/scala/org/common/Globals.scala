package org.common

import org.mavok.common.ConnectionType
import org.sqlGenerator.{ConfigName, JDBCConnection}

/**
 * Copyright: Canusi
 *
 * @author Benjamin Hargrave
 * @since 2019-11-24 
 */
object Globals {


  val TEST_POSTGRES_CONNECTION = JDBCConnection( ConnectionType.POSTGRES, ConfigName( "slick-postgres-meta" ) )


  val TEST_SCHEMA = "scala_testing"
}
