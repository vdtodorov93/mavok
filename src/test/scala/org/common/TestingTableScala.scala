package org.common




/*
 * Copyright: blacktest -- created 24.08.19
 * Documentation: https://devhat.atlassian.net/wiki/spaces/ADM/overview
 */
  /*
import slick.driver.PostgresDriver.api._
import slick.lifted.TableQuery

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.Future
import slick.lifted.Tag

/**
  * @author Ondřej Kratochvíl
  */
case class Category(id: Long, name: String)

case class CategoryFormData(id: Long, name: String)


class CategoryTable(tag: Tag) extends Table[Category](tag, "category") {

  def id = column[Long]("id", O.PrimaryKey, O.AutoInc)

  def name = column[String]("name")

  def * = (id, name) <>(Category.tupled, Category.unapply)
}
object DatabaseConnection {
  val connection = Database.forURL("jdbc:postgresql://localhost:5432/taskhelper", "postgres", "postgres")
}



object CategoryService {

  val db = DatabaseConnection.connection
  val categories = TableQuery[CategoryTable]

  def add(category: Category): Future[String] = {
    db.run(categories += category)
      .map(res => "Category successfully added")
      .recover {
        case ex: Exception => ex.getCause.getMessage
      }
  }

  def saveOrUpdate(category: Category): Future[String] = {
    if (category.id == 0) {
      add(category)
    } else {
      db.run(categories
        .filter(_.id === category.id)
        .update(category))
        .map(res => s"Category $category.id successfully updated")
        .recover {
          case ex: Exception => ex.getCause.getMessage
        }
    }
  }

  def delete(id: Long): Future[Int] = {
    db.run(categories
      .filter(_.id === id)
      .delete)
  }

  def get(id: Long): Future[Option[Category]] = {
    db.run(categories
      .filter(_.id === id)
      .result
      .headOption)
  }

  def listAll: Future[Seq[Category]] = {
    db.run(categories
      .sortBy( result => ( result.id, result.id ) )
      .result)
  }
}
*/


