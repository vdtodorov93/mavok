package org.common

/*
 * Copyright: blacktest -- created 11.08.19
 * Documentation: https://devhat.atlassian.net/wiki/spaces/ADM/overview
 */


import org.mavok.api.sql.DMLBuilder
import org.mavok.api.table.TableModel
import org.mavok.common.ConnectionType
import org.mavok.datatype.dbtype.{DBColumnType, STRING}
import org.scalatest.{FlatSpec, GivenWhenThen, Matchers}
import org.testModels.{Table2Columns, Table3Columns, TableModelTest}
import testEngine.TableBuilder


class test_LogicQuery extends FlatSpec with Matchers with GivenWhenThen  with TableBuilder {

  override val tableList: Vector[TableModel[_ <: Product]] = Vector( TableModelTest )


  val expectedResult = Array(
    "SELECT", " ", " ", "zzz1", ".", "orange", " ", "zzz3", "\n"
    , ",", " ", "CAST", "(", " ", " ", "zzz1", ".", "apple", " ", "\n"
    , " ", "AS", " ", "VARCHAR2", "(", " ", "255", " ", ")", ")", " ", "zzz4", "\n"
    , " ", "FROM", " ", "Test.orange", " ", "zzz1", " ", "\n"
    , "LEFT OUTER", " ", "test.test", " ", "zzz2", " ", "\n"
    , "ON", " ", " ", "(", " ", "zzz1", ".", "orange", " ", "\n"
    , ")", " ", "\n"
    , " = ", " ", "(", " ", "zzz2", ".", "intColumn", " ", "\n"
    , ")", " ", " ", "\n"
    , "WHERE", " ", "(", " ", "zzz2", ".", "intColumn", " ", "\n"
    , ")", " ", "\n"
    , " = ", " ", "(", " ", "zzz1", ".", "orange", " ", "\n"
    , ")", " "

  )
  // ---------------------  Tests


  "Query" should "match array" in {


    val connection = ConnectionType.ORACLE


    val buildSql =
      DMLBuilder( Table3Columns )
        .select( Table3Columns.colString, Table3Columns.colInt.as[STRING](DBColumnType.VARCHAR( 255 )) )

        .left( Table2Columns )
        .on( Table3Columns.colString :== Table2Columns.colInt )
        .where( Table3Columns.colInt :==  Table2Columns.colString )
        .buildSQLBlocks( connection ).map( _.value )

    validateListsMatch( buildSql, expectedResult )

    info( "Query built successfully: " + expectedResult.mkString )
  }

  def validateListsMatch( leftSeq: Seq[Any], rightSeq: Seq[Any] ) = {

    for( i <- leftSeq.indices ){
      assert( leftSeq(i) == rightSeq(i), s"Correct so far - ${leftSeq.take(i).mkString}" +
        s". Position ${i} did not match. ${leftSeq(i)} != ${rightSeq(i)}")
    }

  }


}
