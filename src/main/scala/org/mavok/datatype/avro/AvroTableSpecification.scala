package org.mavok.datatype.avro

import org.mavok.datatype.Avro2DBColumnMapping

/**
  * AvroTableSpecification provides a way to expand on the recordDefintion with Avro.
  * The properties object allows you specify 'how' a table may look in different contexts
  * Oracle may require setting up the table in parallel ( for example )
  * These fields can be setup inside
 *
  * @param recordDefinition
  * @param properties
  */
case class AvroTableSpecification(recordDefinition: Avro2DBColumnMapping, properties: Avro2DBColumnMapping )
