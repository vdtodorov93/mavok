package org.mavok.datatype.avro.logicalType

import org.apache.avro.{LogicalType, LogicalTypes, Schema}

object AvroLogicalTypeRegister {


  def registerLogicalTypes(): Unit = {
    registerLogicalVarchar()
  }


  private def registerLogicalVarchar(): Unit = {
    LogicalTypes.register(LOGICAL_TYPE_VARCHAR.toString, new LogicalTypes.LogicalTypeFactory() {
      private val logicalVarchar: LogicalType = LogicalVarchar(255)

      override def fromSchema(schema: Schema): LogicalType = {
        logicalVarchar
      }
    }
    )
  }
}
