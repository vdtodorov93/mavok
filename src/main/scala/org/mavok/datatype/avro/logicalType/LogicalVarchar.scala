package org.mavok.datatype.avro.logicalType

import java.nio.ByteBuffer

import org.apache.avro.LogicalType

/*
 * Copyright: blacktest -- created 22.08.19
 * Documentation: https://devhat.atlassian.net/wiki/spaces/ADM/overview
 */


import org.apache.avro.Schema

object LogicalVarchar {
  // Construct a unique instance for all the conversion. This have to be changed in case the conversion
  //   needs some runtime information (e.g.: an encryption key / a tenant_ID). If so, the get() method should
  //   return the appropriate conversion per key.
  private val INSTANCE = LogicalVarchar( 255 )
  private val LENGTH_PROP = "length"

  def apply( _length: Int ): LogicalVarchar = new LogicalVarchar() {
    override val length: Int = _length
  }

  def get: LogicalVarchar = INSTANCE





  /*
  @Override
  public Conversion<?> getConversion(int fieldIndex) {
    // This allow us to have a more flexible conversion retrieval, so we don't have to code it per field.
    Schema fieldSchema = SCHEMA$.getFields().get(fieldIndex).schema();
    if ((fieldSchema.getLogicalType() != null)
      && (fieldSchema.getLogicalType().getName() == EncryptedLogicalType.ENCRYPTED_LOGICAL_TYPE_NAME)){
      // here we could pass to the get() method a runtime information, e.g.: a tenantId that can be found in the data structure.
      return EncryptedConversion.get();
    }
    return null;
  }*/

}



abstract class LogicalVarchar extends LogicalType( LOGICAL_TYPE_VARCHAR.toString ) {

  val length: Int

  override def addToSchema(schema: Schema): Schema = {
    super.addToSchema(schema)
    schema.addProp(LogicalVarchar.LENGTH_PROP, length )
    schema
  }



  override def equals(thatString: Any): Boolean = {
    if (this == thatString) return true
    thatString match {
      case string: LogicalVarchar => length == string.length
      case _ => false
    }

  }

  override def validate(schema: Schema): Unit = {
    super.validate(schema)
    if ( ( schema.getType != Schema.Type.FIXED ) && ( schema.getType != Schema.Type.BYTES ) ) {
      throw new IllegalArgumentException("Logical type length must be backed by fixed or bytes")
    }
    if( this.length <= 0 ){
      throw new IllegalArgumentException("Invalid length: " + length + " (must be positive)")
    }
  }



  override def hashCode: Int = {
    LOGICAL_TYPE_VARCHAR.hashCode + length
  }

  import org.apache.avro.{Conversion, LogicalType}



  class LogicalVarcharConversion private() extends Conversion[ByteBuffer] {
    //This conversion operates on ByteBuffer and returns ByteBuffer
    override def getConvertedType: Class[ByteBuffer] = classOf[ByteBuffer]
    override def getLogicalTypeName: String = LOGICAL_TYPE_VARCHAR.toString

    // fromBytes and toBytes have to be overridden as this conversion works on bytes. Other may need to be
    //  overridden. The types supported need to be updated also in EncryptedLogicalType#validate(Schema schema)
    override def fromBytes(value: ByteBuffer, schema: Schema, `type`: LogicalType): ByteBuffer = {
      value
    }

    override def toBytes(value: ByteBuffer, schema: Schema, `type`: LogicalType): ByteBuffer = {
      value
    }
  }
}
