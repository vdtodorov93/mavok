package org.mavok.datatype.avro.logicalType

import org.apache.avro.LogicalType

/*
 * Copyright: blacktest -- created 22.08.19
 * Documentation: https://devhat.atlassian.net/wiki/spaces/ADM/overview
 */


import org.apache.avro.Schema

object LogicalDecimal {

  private val PRECISION_PROP = "precision"
  private val SCALE_PROP = "scale"
  private val LOGICAL_DECIMAL_TYPE = "LOGICAL_DECIMAL_TYPE"

  def apply( _precision: Int, _scale: Int): LogicalDecimal = new LogicalDecimal() {

    override val precision: Int = _precision
    override val scale: Int = _scale

  }

}

abstract class LogicalDecimal extends LogicalType( LogicalDecimal.LOGICAL_DECIMAL_TYPE ) {

  val precision: Int
  val scale: Int

  override def addToSchema(schema: Schema): Schema = {
    super.addToSchema(schema)
    schema.addProp(LogicalDecimal.PRECISION_PROP, precision)
    schema.addProp(LogicalDecimal.SCALE_PROP, scale)
    schema
  }



  override def equals(thatDecimal: Any): Boolean = {
    if (this == thatDecimal) return true

    thatDecimal match {
      case decimal: LogicalDecimal => precision == decimal.precision && scale == decimal.scale
      case _ => false
    }

  }

  override def validate(schema: Schema): Unit = {
    super.validate(schema)
    val max = maxPrecision( schema )
    0 match {
      case v if ( schema.getType != Schema.Type.FIXED ) && ( schema.getType != Schema.Type.BYTES ) => throw new IllegalArgumentException("Logical type decimal must be backed by fixed or bytes")
      case v if this.precision <= 0 => throw new IllegalArgumentException("Invalid decimal precision: " + precision + " (must be positive)")
      case v if this.precision > max => throw new IllegalArgumentException("fixed(" + schema.getFixedSize + ") cannot store " + precision + " digits (max " + max + ")")
      case v if this.scale < 0 => throw new IllegalArgumentException("Invalid decimal scale: " + scale + " (must be positive)")
      case v if this.scale > this.precision => throw new IllegalArgumentException("Invalid decimal scale: " + scale + " (greater than precision: " + precision + ")")
    }
  }


  private def maxPrecision(schema: Schema): Long = {
    0 match {
      case v if schema.getType == Schema.Type.BYTES => Integer.MAX_VALUE
      case v if schema.getType == Schema.Type.FIXED => Math.round(          // convert double to long
        Math.floor(Math.log10(  // number of base-10 digits
          Math.pow(2, 8 * schema.getFixedSize - 1) - 1)  // max value stored
        ))
      case _ => 0
    }
  }

  override def hashCode: Int = {
    "LOGICAL_DECIMAL".hashCode + precision + scale
  }
}
