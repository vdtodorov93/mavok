package org.mavok.datatype.dbtype

import org.mavok.shapeLibrary.rdms.column.dataType._

case class DBColumnType[Type <: DataType](
                         typePattern: DataPattern
                       , toDDL: AbstractDDLType
                     ){

}


object DBColumnType {

  def VARCHAR( precision : Int): DBColumnType[STRING] = {
    DBColumnType[STRING]( typePattern = DBTypeString( precision ), toDDL = DDLVarchar( precision ))
  }

  def BOOLEAN: DBColumnType[BOOLEAN]  = {
    DBColumnType[BOOLEAN](typePattern = DBTypeBoolean(), toDDL = DDLBoolean() )
  }

  def INTEGER: DBColumnType[INTEGER]  = {
    DBColumnType[INTEGER](typePattern = DBTypeInteger(), toDDL = DDLInteger() )
  }

  def DOUBLE(width: Int, scale: Int): DBColumnType[DOUBLE]  = {
    DBColumnType[DOUBLE](typePattern = DBTypeFloat( width, scale ), toDDL = DDLDouble() )
  }

  def BIGINT: DBColumnType[BIGINT]  = {
    DBColumnType[BIGINT](typePattern = DBTypeBigint(), toDDL = DDLNumber(38 , 0 ) )
  }

  def NUMBER( precision : Int, scale: Int ): DBColumnType[NUMBER]  = {
    DBColumnType[NUMBER](typePattern = DBTypeBigint(), toDDL = DDLNumber( precision, scale ) )
  }


  def LONG: DBColumnType[LONG]  = {
    DBColumnType[LONG](typePattern = DBTypeLong(), toDDL = DDLLong() )
  }

  def DATE: DBColumnType[DATE]  = {
    DBColumnType[DATE](typePattern = DBTypeDate(), toDDL = DDLDate())
  }

  def TIMESTAMP: DBColumnType[TIMESTAMP]  = {
    DBColumnType[TIMESTAMP](typePattern = DBTypeDate(), toDDL = DDLTimestamp())
  }

  def FLOAT( precision : Int, scale: Int ): DBColumnType[FLOAT]  = {
    DBColumnType[FLOAT](typePattern = DBTypeFloat( precision, scale ), toDDL = DDLDouble())
  }






  def defaultValue( columnType: DBColumnType[_]  ): String = {

    columnType.typePattern.primitiveType match {

      case PrimitiveType.BIGINT | PrimitiveType.INTEGER | PrimitiveType.DOUBLE | PrimitiveType.NUMBER | PrimitiveType.FLOAT  => "-9995544"
      case PrimitiveType.STRING => "'-9995544'"
      case PrimitiveType.DATE | PrimitiveType.TIMESTAMP => "to_date( '0001-01-01', 'yyyy-mm-dd' )"
      case _ => "null"

    }

  }



}

