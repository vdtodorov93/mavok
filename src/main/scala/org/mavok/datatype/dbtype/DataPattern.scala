package org.mavok.datatype.dbtype

/*
 * Copyright: blacktest -- created 08.01.19
 * Documentation: https://devhat.atlassian.net/wiki/spaces/ADM/overview
 */

object FloatSeparator extends Enumeration {
  val PERIOD = Value( ".")
  val COMMA = Value( ",")
}


object DateFormat extends Enumeration {
  val YYYYMMDD = Value( "YYYYMMDD")
  val YYYYMONDD = Value( "YYYYMONDD")
  val YYYYMMDD_HH24MISSFF = Value( "YYYYMMDD HH24MISSFF")
}


protected trait DataPattern extends DataType { val width: Int  }
final class DBTypeBoolean extends DataPattern with BOOLEAN{ override val width: Int = 1 }
final class DBTypeInteger extends DataPattern with INTEGER { val width: Int = 4 }
final class DBTypeLong extends DataPattern with LONG { val width: Int = 8 }
final class DBTypeBigint extends DataPattern with BIGINT { val width: Int = 16 }
abstract class DBTypeString extends DataPattern with STRING
abstract class DBTypeFloat extends DataPattern with FLOAT {
  val scale: Int
  val separator: FloatSeparator.Value }

abstract class DBTypeDate extends DataPattern with DATE {
  val dateFormat: DateFormat.Value }





object DBTypeBoolean{ def apply(): DBTypeBoolean =  new DBTypeBoolean() }
object DBTypeString { def apply(_width: Int): DBTypeString =  new DBTypeString { val width: Int = _width } }
object DBTypeBigint{ def apply(): DBTypeBigint = new DBTypeBigint() }
object DBTypeLong{ def apply(): DBTypeLong = new DBTypeLong() }
object DBTypeInteger{ def apply(): DBTypeInteger = new DBTypeInteger() }

object DBTypeFloat{ def apply(_width: Int, _scale: Int, _separator: FloatSeparator.Value = FloatSeparator.PERIOD ): DBTypeFloat =
  new DBTypeFloat{
      val scale: Int = _scale
      val separator: FloatSeparator.Value = _separator
      val width: Int = _width } }


object DBTypeDate{ def apply(_dateFormat: DateFormat.Value = DateFormat.YYYYMMDD_HH24MISSFF ): DBTypeDate =
  new DBTypeDate { val dateFormat: DateFormat.Value = _dateFormat
    override val width: Int = dateFormat.toString.length
  } }