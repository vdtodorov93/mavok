package org.mavok.datatype

import org.apache.avro.Schema
import org.mavok.LOG
import org.mavok.api.table.DatasetPersistent
import org.mavok.common.classvalue.{FilePath, Name, StringAvroDefinition}
import org.mavok.datatype.avro.logicalType.AvroLogicalTypeRegister
import org.mavok.datatype.dbtype.{DBColumnType, DataType}

import scala.collection.mutable.ListBuffer
import scala.io.Source
import scala.util.{Failure, Success, Try}

/*
 * Copyright: blacktest -- created 10.05.19
 * Documentation: https://devhat.atlassian.net/wiki/spaces/ADM/overview
 */






object Avro2DBColumnMapping {

  AvroLogicalTypeRegister.registerLogicalTypes()

  def apply( avroSchemaString: StringAvroDefinition )( implicit dummy: DummyImplicit ): Avro2DBColumnMapping = {

    // try to build schema
    val avroSchemaBuilder = Try( new Schema.Parser().parse(avroSchemaString.schema) )
    val _schema: Schema = avroSchemaBuilder match {
      case Success( v ) => v
      case Failure( e ) =>  LOG.error( "Failed to build avro schema", s"Using String: ${avroSchemaString}" ); throw e
    }

    // get ordered fields
    val iterator = _schema.getFields.iterator()
    val unorderedFields = ListBuffer[Schema.Field]()
    while( iterator.hasNext ){ unorderedFields += iterator.next() }
    val orderedFields = unorderedFields.sortBy( _.pos() )


    val _fieldNames: ListBuffer[Name] = orderedFields.map( field => Name(  field.name() ) )
    val _fieldTypes: ListBuffer[DBColumnType[_ <: DataType]] = orderedFields.map(field => TypeConverter.mapAvroField2DBColumnType( field.schema() ) )

    new Avro2DBColumnMapping {
      val name: Name = Name( _schema.getFullName )
      val schema: Schema = _schema
      val fieldNames: Vector[Name] = _fieldNames.toVector
      val fieldTypes: Vector[DBColumnType[_ <: DataType]] = _fieldTypes.toVector
    }

  }

  def fromAvroFile( path: FilePath ): Avro2DBColumnMapping = {

    val tryFileLoad = Try( Source.fromInputStream( getClass.getResourceAsStream( path.path ) ) )
    val file = tryFileLoad match{
      case Success( value ) => value
      case Failure( e ) => LOG.error( "Failed to parse file", s"Using filepath: ${path.path}" ); throw e
    }

    val schemaString = StringAvroDefinition( file.getLines().mkString )
    Avro2DBColumnMapping( schemaString )
  }






  def fromTable(table: DatasetPersistent ) : Avro2DBColumnMapping = {

    require( table != null, "Table input not allowed to be null")


    val header: String  =
      """ {
        |         "type" : "record",
        |         "namespace" : """".stripMargin + table.tablePath.uuid +
        """" ,
          |         "name" : """".stripMargin + table.tablePath.getTablePath + """",
         "fields": [
                 """
    val footer =
      """]
        |}""".stripMargin

    val avroSchemaBuilder: StringBuilder  = new StringBuilder()
    avroSchemaBuilder.append( header )

    //todo Column Names/Pointers need to be more indepedent of columns than they currently are
    val columns = table.getColumns
    val avroFields = columns.map{
      column => {
        val columnType = column.dbType

        val sourceColumn = column
        val columnName: String  = sourceColumn.name.name.toLowerCase()

        """{
          |     "name": """".stripMargin + columnName + """"
                                                          |   , "type": """.stripMargin + TypeConverter.mapDBColumnType2AvroField( columnType ) +
          """
            |} """.stripMargin
      }
    }.mkString( ", ")


    avroSchemaBuilder.append( avroFields )
    avroSchemaBuilder.append( footer )

    Avro2DBColumnMapping( StringAvroDefinition( avroSchemaBuilder.toString() ) )
  }




}



trait Avro2DBColumnMapping {

  val name: Name
  val schema: Schema
  val fieldNames: Vector[Name]
  val fieldTypes: Vector[DBColumnType[_ <: DataType]]


}







