package org.mavok.workflowEngine

import org.mavok.shapeLibrary.rdms.column.comparison.{ColumnComparison, ComparisonGraph}
import org.mavok.shapeLibrary.rdms.keywords._

/*
 * Copyright: blacktest -- created 26.06.19
 * Documentation: https://devhat.atlassian.net/wiki/spaces/ADM/overview
 */

object ColumnCompositeComparisonFlow {

  def apply( expressionColumnGraph: ColumnComparison ): ColumnCompositeComparisonFlow
    =  new ColumnCompositeComparisonFlow(expressionColumnGraph)

}



class ColumnCompositeComparisonFlow private( val expressionColumnGraph: ColumnComparison ) {


  def &&(nextComparison: ColumnCompositeComparisonFlow ): ColumnCompositeComparisonFlow = {
    ColumnCompositeComparisonFlow( ComparisonGraph( expressionColumnGraph, _and , nextComparison.expressionColumnGraph ) )
  }


  def ||(nextComparison: ColumnCompositeComparisonFlow  ): ColumnCompositeComparisonFlow = {
    ColumnCompositeComparisonFlow( ComparisonGraph( expressionColumnGraph, _or , nextComparison.expressionColumnGraph ) )
  }

}
