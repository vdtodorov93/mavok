package org.mavok.workflowEngine.rdbms.alter

object DefaultConstraintName extends ConstraintName( "" )
case class ConstraintName( name: String )

/*
class AlterColumnFlow[T <: AbstractTable]( _table: T ) extends Workflow[Mapping] {


  def addConstraintPrimaryKey( constraintName: ConstraintName, columns: Column[ _ <: DataType, T ]* ) = {
    new AlterTablePrimaryKeyCreate {
      override val table: AbstractTable = _table
      override val graphScope: Scope = Scope()
    }
  }



  def dropConstraintPrimaryKey() = {

  }

  override def assembleGraph(scope: Scope): Mapping = ???
}
*/