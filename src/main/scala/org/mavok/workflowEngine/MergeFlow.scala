package org.mavok.workflowEngine

import org.mavok.api.column.{AbstractColumn, Column}
import org.mavok.shapeLibrary.rdms.column.comparison.ComparisonGraph
import org.mavok.shapeLibrary.rdms.dml.Query
import org.mavok.shapeLibrary.shapeResource.ColumnContainerGraphShape


/*
 * You deliver the impossible
 * Copyright: blacktest -- created 25.05.19
 * Documentation: https://devhat.atlassian.net/wiki/spaces/ADM/overview
 */


abstract class MergeFlow extends Workflow[ColumnContainerGraphShape]   {


  protected var selectColumns: Seq[AbstractColumn[_, _]] = _
  protected var groupColumns: Seq[AbstractColumn[_, _]] = _
  protected var whereColumns: ComparisonGraph = _
  protected var orderColumns: Seq[AbstractColumn[_, _]] = _

  protected val buildQuery: Query


  def mergeMappings( columnMappings: ( AbstractColumn[_, _], Column[_, _])* ): MergeFlow = {
    this
  }


  def merge( columnMappings: AbstractColumn[_, _]* ): MergeFlow = {
    this
  }






  def on( columnCompositeComparisonFlow: ColumnCompositeComparisonFlow ): MergeFlow = {
    this
  }




}







