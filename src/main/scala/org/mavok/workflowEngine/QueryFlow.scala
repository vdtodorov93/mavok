package org.mavok.workflowEngine

import org.mavok.api.column.{AbstractColumn, Column}
import org.mavok.api.table.DatasetReadable
import org.mavok.common.tools.Memoize
import org.mavok.scope.Scope
import org.mavok.shapeLibrary.rdms.column.comparison.ColumnComparison
import org.mavok.shapeLibrary.rdms.dml.Query
import org.mavok.shapeLibrary.rdms.keywords.{INNER_JOIN, LEFT_JOIN, RIGHT_JOIN}

import scala.collection.mutable.ListBuffer


/*
 * You deliver the impossible
 * Copyright: blacktest -- created 25.05.19
 * Documentation: https://devhat.atlassian.net/wiki/spaces/ADM/overview
 */


class QueryFlow[T <:  DatasetReadable[_]](val table: T ) extends Workflow[Query]   {


  // todo fix this
  protected[workflowEngine] var selectColumns: Seq[AbstractColumn[_,_]] = _
  protected[workflowEngine] val listJoins: ListBuffer[JoinFlow[T,_]] = ListBuffer[JoinFlow[T,_]]()
  private val whereColumns: Memoize[ColumnComparison, ColumnComparison]  = Memoize((input: ColumnComparison)  => { input} )
  private val groupColumns: Memoize[Seq[AbstractColumn[_,_]], Seq[AbstractColumn[_,_]]] = Memoize((input: Seq[AbstractColumn[_,_]])  => { input} )
  private val orderColumns: Memoize[Seq[AbstractColumn[_,_]], Seq[AbstractColumn[_,_]]] = Memoize((input: Seq[AbstractColumn[_,_]])  => { input} )




  def select( columns: AbstractColumn[_,_]* ): QueryFlow[T] = {
    selectColumns = columns
    require( selectColumns.nonEmpty)
    this
  }




  def where( comparison: ColumnCompositeComparisonFlow ): QueryFlow[T] = {
    whereColumns.apply( comparison.expressionColumnGraph )
    this
  }

  def inner[R <: DatasetReadable[AbstractColumn[_, _]]](table: R ): JoinFlow[T,R] = {
    JoinFlow[T, R]( this, table, INNER_JOIN )
  }

  def left[R <: DatasetReadable[AbstractColumn[_, _]]](table: R ): JoinFlow[T,R] = {
    JoinFlow[T, R]( this, table, LEFT_JOIN )
  }

  def right[R <: DatasetReadable[AbstractColumn[_, _]]](table: R ): JoinFlow[T,R] = {
    JoinFlow[T, R]( this, table, RIGHT_JOIN )
  }




  def update[ S <:  DatasetReadable[_]](targetTable: S )(headers: Column[_,_]* ): UpdateFlow[S] = {
    UpdateFlow[S]( targetTable, this ).apply( headers : _* )
  }




  def groupBy(  columns: AbstractColumn[_, _]*) : QueryFlow[T] = {
    groupColumns.apply( columns )
    this
  }




  def orderBy[G <: AbstractColumn[_, _]](columns: AbstractColumn[_, _]*) : QueryFlow[T] = {
    orderColumns.apply( columns )
    this
  }



  
  def insert[ S <:  DatasetReadable[_] ](targetTable: S )(columns: Column[_, _ <: S]* ): InsertFlow[S] = {
    InsertFlow[S]( targetTable, assembleGraph() ).insertBasic( columns :_ *)
  }

  def union( query: QueryFlow[T]): QueryFlow[T] = {
    this
  }





  override def assembleGraph( scope: Scope = Scope()  ): Query = {
    Query(
      scope
      , _selectColumns = selectColumns
      , _listJoins = listJoins.map( _.assembleGraph( scope ) )
      , _whereColumns = whereColumns.getOption()
      , _groupColumns = groupColumns.getOption()
      , _orderColumns = orderColumns.getOption()
      , _source = table.asShape
    )
  }



}


object QueryFlow {
  def apply[ T <: DatasetReadable[_]](table: T ): QueryFlow[T] = new QueryFlow( table )
}





