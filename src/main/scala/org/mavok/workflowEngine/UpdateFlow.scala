package org.mavok.workflowEngine

import org.mavok.api.column.{AbstractColumn, Column}
import org.mavok.api.table.DatasetReadable
import org.mavok.common.tools.Memoize
import org.mavok.scope.Scope
import org.mavok.shapeLibrary.rdms.dml.Update

/*
 * You deliver the impossible
 * Copyright: blacktest -- created 25.05.19
 * Documentation: https://devhat.atlassian.net/wiki/spaces/ADM/overview
 */

class UpdateFlow[ S <: DatasetReadable[_]](source: S, query: QueryFlow[_] )  extends Workflow[Update]  {

  private val columnMappings = Memoize( ( param: Seq[(Column[_, _], AbstractColumn[_, _])] ) => { param } )
  private val columnCompositeComparisonFlowMem = Memoize( ( param: ColumnCompositeComparisonFlow ) => { param } )


  def applyMap( columns: ( AbstractColumn[_, _], AbstractColumn[_, _])* ): UpdateFlow[S] = {
    this
  }



  def apply( columns: Column[_, _]* ): UpdateFlow[S] = {
    //todo
    onMap( ( columns zip query.selectColumns ):_* )
    this
  }



  def onMap( columns: ( Column[_, _ ], AbstractColumn[_, _])* ): UpdateFlow[S] = {
    columnMappings( columns )
    this
  }

  def on( comparison: ColumnCompositeComparisonFlow ): UpdateFlow[S] = {
    columnCompositeComparisonFlowMem( comparison )
    this
  }



  override def assembleGraph( scope: Scope = Scope()   ): Update = {
    Update( query, columnCompositeComparisonFlowMem.get(), columnMappings.get() )
  }
}


object UpdateFlow {

  def apply[S <: DatasetReadable[_]](source: S, query: QueryFlow[_] ): UpdateFlow[S] = new UpdateFlow[S]( source, query )
}



