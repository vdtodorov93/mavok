package org.mavok.workflowEngine

import org.mavok.api.column.{AbstractColumn, ColumnExtendConcat, ColumnExtendGetOrElse, ColumnInterface}
import org.mavok.api.table.DatasetReadable
import org.mavok.datatype.dbtype.{DBColumnType, DataType, STRING}
import org.mavok.shapeLibrary.rdms.column.common.{ColumnHeaderInitParams, ColumnReferenceHeader}
import org.mavok.shapeLibrary.rdms.column.comparison.{ComparisonGraph, SingleOperatorGraph}
import org.mavok.shapeLibrary.rdms.column.extendedtypes.{ColumnExtendCast, ColumnExtendCount, ColumnExtendMax, ColumnExtendMin}
import org.mavok.shapeLibrary.rdms.keywords._

import scala.collection.mutable.ListBuffer
/*
 * Copyright: blacktest -- created 26.06.19
 * Documentation: https://devhat.atlassian.net/wiki/spaces/ADM/overview
 */

object ColumnFlow {

  // ---------------------  Constructors

  def apply[T <: DataType]( columnShape: AbstractColumn[T, _ <: DatasetReadable[_]] ): ColumnFlow[T] = {
    new ColumnFlow( columnShape )
  }

}


class ColumnFlow[T <: DataType] private( val columnShape: AbstractColumn[T, _ <: DatasetReadable[_]] ) extends ColumnInterface{

  def isNotLike(that: AbstractColumn[_ <: DataType, _ <: DatasetReadable[_]]): ColumnCompositeComparisonFlow = ???


  def isNotNull: ColumnCompositeComparisonFlow = ???


  def getOrElse[S >: T <: DataType](that: AbstractColumn[S, _ <: DatasetReadable[_]]): ColumnExtendGetOrElse[S] = {
    new ColumnExtendGetOrElse[S]( columnShape, that )
  }


  import org.mavok.api.Implicits._
  override def :==( that: AbstractColumn[_ <: DataType, _ <: DatasetReadable[_]] ): ColumnCompositeComparisonFlow = {
    ColumnCompositeComparisonFlow( ComparisonGraph( generateHeader( columnShape ), _equals, generateHeader( that ) ) )
  }


  override def :!=( that: AbstractColumn[_ <: DataType, _ <: DatasetReadable[_]] ): ColumnCompositeComparisonFlow = {
    ColumnCompositeComparisonFlow( ComparisonGraph( generateHeader( columnShape ), _notEquals , generateHeader( that ) ) )
  }


  override def :>( that: AbstractColumn[_ <: DataType, _ <: DatasetReadable[_]] ): ColumnCompositeComparisonFlow = {
    ColumnCompositeComparisonFlow( ComparisonGraph( generateHeader( columnShape ), _greater, generateHeader( that ) ) )
  }

  override def :<( that: AbstractColumn[_ <: DataType, _ <: DatasetReadable[_]] ): ColumnCompositeComparisonFlow = {
    ColumnCompositeComparisonFlow( ComparisonGraph( generateHeader( columnShape ), _less, generateHeader( that ) ) )
  }



  override def :<=( that: AbstractColumn[_ <: DataType, _ <: DatasetReadable[_]] ): ColumnCompositeComparisonFlow = {
    ColumnCompositeComparisonFlow( ComparisonGraph( generateHeader( columnShape ), _lessEquals , generateHeader( that ) ) )
  }

  override def :>=( that: AbstractColumn[_ <: DataType, _ <: DatasetReadable[_]] ): ColumnCompositeComparisonFlow = {
    ColumnCompositeComparisonFlow( ComparisonGraph( generateHeader( columnShape ), _greaterEquals, generateHeader( that ) ) )
  }

  override def :==( that: String ): ColumnCompositeComparisonFlow = {
    this.:==( convertStringToColumn( that ) )
  }



  override def :!=( that: String ): ColumnCompositeComparisonFlow = {
    this.:!=( convertStringToColumn( that ) )
  }


  private def generateHeader( baseColumn: AbstractColumn[_ <: DataType, _ <: DatasetReadable[_]] ) = {
    ColumnReferenceHeader( baseColumn, ColumnHeaderInitParams( addAlias = false, addComma = false, addSourceAlias = true ) )
  }


  override def as[S <: DataType ](dbColumnType: DBColumnType[S] ): ColumnExtendCast[S] = {
      new ColumnExtendCast[S]( columnShape, dbColumnType )
  }



  override def isNull = {
    ColumnCompositeComparisonFlow( SingleOperatorGraph( generateHeader( columnShape ), _isNull ) )
  }

  override def isLike( that: AbstractColumn[_ <: DataType, _ <: DatasetReadable[_]] ) : ColumnCompositeComparisonFlow = {
    import org.mavok.api.Implicits._
    // todo put in its own class
    val likeClassColumn = "%" concat that concat "%"

    ColumnCompositeComparisonFlow( ComparisonGraph( generateHeader( columnShape ), _like, generateHeader( likeClassColumn ) ) )
  }


  override def concat(that: AbstractColumn[_ <: DataType, _ <: DatasetReadable[_]]*): ColumnExtendConcat[STRING] = {

    val listConcats: ListBuffer[AbstractColumn[_ <: DataType, _ <: DatasetReadable[_]]]
    = ListBuffer[AbstractColumn[_ <: DataType, _ <: DatasetReadable[_]]]()

    listConcats += this.columnShape

    that.foreach{
      listConcats += new ColumnExtendConcat( listConcats.last , _  )
    }

    listConcats.last.asInstanceOf[ColumnExtendConcat[STRING]]
  }

  override def count: ColumnExtendCount = {
    new ColumnExtendCount( columnShape )
  }

  override def min: ColumnExtendMin = {
    new ColumnExtendMin( columnShape )
  }

  override def max: ColumnExtendMax = {
      new ColumnExtendMax( columnShape )
  }
}
