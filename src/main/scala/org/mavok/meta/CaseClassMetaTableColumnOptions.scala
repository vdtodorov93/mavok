package org.mavok.meta

import org.mavok.api.table.QueryModelGeneric
import org.mavok.workflowEngine.QueryFlow

/*
 * Copyright: blacktest -- created 23.08.19
 * Documentation: https://devhat.atlassian.net/wiki/spaces/ADM/overview
 */


object MetaTableColumnOptions extends QueryModelGeneric {
  override val tableQuery: QueryFlow[_] = null
}




/*
case class CaseClassMetaTableColumns( tablePath: String, columnName: String, columnTypeName: String, length: Int, numericWidth: Int, numericScale: Int )

trait MetaTableColumnInterface extends TableQuery {


  val colTablePath: BaseColumn[STRING]
  val colColumnName: BaseColumn[STRING]
  val isPrimaryKey: BaseColumn[INTEGER]
  val isUnique: BaseColumn[INTEGER]
  val isNotNull: BaseColumn[INTEGER]

}


private object PostgresAllTables extends Table{
  override val tablePath: HasTablePath = TablePath( "information_schema", "columns" )

  val colSchemaName: Column[STRING] = column[STRING]( "table_schema")
  val colTableName: Column[STRING] = column[STRING]( "table_name")
  val colColumnName: Column[STRING] = column[STRING]( "column_name")
  val colColumnTypeName: Column[STRING] = column[STRING]( "data_type")
  val colLength: Column[INTEGER] = column[INTEGER]( "character_octet_length")
  val colNumericWidth: Column[INTEGER] = column[INTEGER]( "numeric_precision")
  val colNumericScale: Column[INTEGER] = column[INTEGER]( "numeric_scale")

}

object PostgresColumnInterface extends MetaTableColumnInterface {

  import org.mavok.api.Implicits._
  override val colTablePath: BaseColumn[STRING] = column[STRING]( PostgresAllTables.colSchemaName concat "'.'"  concat( PostgresAllTables.colTableName ) )
  override val colColumnName: BaseColumn[STRING] = column[STRING]( PostgresAllTables.colColumnName )
  override val colColumnTypeName: BaseColumn[STRING] = column[STRING]( PostgresAllTables.colColumnTypeName)
  override val colLength: BaseColumn[INTEGER] = column[INTEGER]( PostgresAllTables.colLength )
  override val colNumericWidth: BaseColumn[INTEGER] = column[INTEGER](  PostgresAllTables.colNumericWidth )
  override val colNumericScale: BaseColumn[INTEGER] = column[INTEGER](  PostgresAllTables.colNumericScale )

  override lazy val tableQuery: QueryFlow = DMLBuilder( PostgresAllTables ).select( this.getColumns: _* )


}

private object OracleAllTables extends Table{
  override val tablePath: HasTablePath = TablePath( "ALL_TABLES" )

  val colSchemaName: Column[STRING] = column[STRING]( "OWNER")
  val colTableName: Column[STRING] = column[STRING]( "TABLE_NAME")
  val colColumnName: Column[STRING] = column[STRING]( "COLUMN_NAME")
  val colColumnTypeName: Column[STRING] = column[STRING]( "COLUMN_TYPE_NAME")
  val colLength: Column[INTEGER] = column[INTEGER]( "character_length")
  val colNumericWidth: Column[INTEGER] = column[INTEGER]( "numeric_width")
  val colNumericScale: Column[INTEGER] = column[INTEGER]( "numeric_scale")


}

object OracleColumnInterface extends MetaTableColumnInterface {

  import org.mavok.api.Implicits._
  override val colTablePath: BaseColumn[STRING] = OracleAllTables.colSchemaName concat "'.'"  concat( OracleAllTables.colTableName )
  override val colColumnName: BaseColumn[STRING] = column[STRING]( OracleAllTables.colColumnName )
  override val colColumnTypeName: BaseColumn[STRING] = column[STRING]( OracleAllTables.colColumnTypeName)
  override val colLength: BaseColumn[INTEGER] = column[INTEGER]( OracleAllTables.colLength )
  override val colNumericWidth: BaseColumn[INTEGER] = column[INTEGER](  OracleAllTables.colNumericWidth )
  override val colNumericScale: BaseColumn[INTEGER] = column[INTEGER](  OracleAllTables.colNumericScale )

  override lazy val tableQuery: QueryFlow = DMLBuilder( OracleAllTables ).select( this.getColumns: _* )

}


object  MetaTableColumns extends ContractList[MetaTableColumnInterface] {

  override val defaultSchematic: MetaTableColumnInterface = PostgresColumnInterface
  override val schematicOracle12: MetaTableColumnInterface = OracleColumnInterface
  override val schematicPostgres10: MetaTableColumnInterface = PostgresColumnInterface

  def apply( _jdbcPlugin: JDBCPlugin): MetaTableTemplate[CaseClassMetaTableColumns, MetaTableColumnInterface] = {

    require( PostgresColumnInterface.getColumns.nonEmpty )

    val _tableQuery = getSchematicContract( _jdbcPlugin.getConnectionType )
    new MetaTableTemplate[CaseClassMetaTableColumns, MetaTableColumnInterface]() {
      override protected val jdbcPlugin: JDBCPlugin = _jdbcPlugin
      override val tableQueryObject: MetaTableColumnInterface = _tableQuery
    }
  }

}

 */





