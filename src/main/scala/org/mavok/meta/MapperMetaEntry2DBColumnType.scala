package org.mavok.meta

import org.mavok.ContractList
import org.mavok.common.ConnectionType
import org.mavok.datatype.dbtype.{DBColumnType, DataType}

/*
 * Copyright: blacktest -- created 24.08.19
 * Documentation: https://devhat.atlassian.net/wiki/spaces/ADM/overview
 */


object MapperMetaEntry2DBColumnType extends ContractList[MapperMetaEntry2DBColumnType] {

  // ---------------------  Constructors

  def apply( connectionType: ConnectionType.Value ): MapperMetaEntry2DBColumnType = {

    this.getSchematicContract( connectionType )
  }

  override val defaultSchematic: MapperMetaEntry2DBColumnType = new MapPostgres10__2DBColumn()
  override val schematicOracle12: MapperMetaEntry2DBColumnType = new MapOracle12c__2DBColumn()
  override val schematicPostgres10: MapperMetaEntry2DBColumnType = new MapPostgres10__2DBColumn()
}

trait MapperMetaEntry2DBColumnType {

  def convert( column: CaseClassMetaTableColumns ): DBColumnType[_ <: DataType ]


}



class MapPostgres10__2DBColumn extends MapperMetaEntry2DBColumnType {

  def convert( column: CaseClassMetaTableColumns ): DBColumnType[_ <: DataType] = {

    column.columnTypeName.toUpperCase() match {

      case "CHARACTER VARYING" => DBColumnType.VARCHAR( column.length )
      case "INTEGER" => DBColumnType.INTEGER
      case "DATE" => DBColumnType.DATE
      case "TIMESTAMP" => DBColumnType.TIMESTAMP
      case "TIMESTAMP WITHOUT TIME ZONE" => DBColumnType.TIMESTAMP
      case "BIGINT" => DBColumnType.BIGINT
      case "NUMERIC" => DBColumnType.NUMBER( column.numericWidth, column.numericScale )
      case "DOUBLE" => DBColumnType.DOUBLE( column.numericWidth, column.numericScale )
      case "DOUBLE PRECISION" => DBColumnType.DOUBLE( column.numericWidth, column.numericScale )
      case _ => throw new IllegalArgumentException( s"Couldnt match type: ${column.columnTypeName}")
    }

  }


}


class MapOracle12c__2DBColumn extends MapperMetaEntry2DBColumnType {

  def convert( column: CaseClassMetaTableColumns ): DBColumnType[_ <: DataType] = {

    column.columnTypeName.toUpperCase() match {

      case "VARCHAR" => DBColumnType.VARCHAR( column.length )
      case "VARCHAR2" => DBColumnType.VARCHAR( column.length )
      case "BLOB" => DBColumnType.VARCHAR( column.length )
      case "CLOB" => DBColumnType.VARCHAR( column.length )
      case "INTEGER" => DBColumnType.INTEGER
      case "DATE" => DBColumnType.DATE
      case "TIMESTAMP" => DBColumnType.TIMESTAMP
      case "TIMESTAMP WITHOUT TIME ZONE" => DBColumnType.TIMESTAMP
      case "NUMBER" => DBColumnType.NUMBER( column.numericWidth, column.numericScale )
      case _ => throw new IllegalArgumentException( s"Couldnt match type: ${column.columnTypeName}")
    }

  }


}