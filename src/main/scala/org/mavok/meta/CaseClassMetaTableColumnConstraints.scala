package org.mavok.meta

import org.mavok.ContractList
import org.mavok.api.column.{AbstractColumn, Column}
import org.mavok.api.sql.DMLBuilder
import org.mavok.api.table._
import org.mavok.controller.ReadableTableController
import org.mavok.datatype.dbtype.STRING
import org.mavok.jdbc.JDBCPlugin
import org.mavok.workflowEngine.QueryFlow

/*
 * Copyright: blacktest -- created 23.08.19
 * Documentation: https://devhat.atlassian.net/wiki/spaces/ADM/overview
 */





object ColumnConstraintType extends Enumeration {
  def withNameOpt(s: String): Option[Value] = values.find(_.toString == s)
}

case class CaseClassMetaTableColumnConstraints( tablePath: String, columnName: String, columnConstraintType: String )

trait MetaTableColumnConstraintsInterface extends QueryModelGeneric {

  val colTablePath: AbstractColumn[STRING,_ <: DatasetReadable[_]]
  val colColumnName: AbstractColumn[STRING,_ <: DatasetReadable[_]]
  val colColumnConstraintType: AbstractColumn[STRING,_ <: DatasetReadable[_]]

}


private object PostgresTableColumnConstraints extends AbstractTable{
  override val tablePath: HasTablePath = TablePath(
    """
      |( select kcu.table_schema schema_name,
      |       kcu.table_name,
      |       kcu.column_name as column_name,
      |        'PRIMARY_KEY'  CONSTRAINT_TYPE
      |from information_schema.table_constraints tco
      |join information_schema.key_column_usage kcu
      |     on kcu.constraint_name = tco.constraint_name
      |     and kcu.constraint_schema = tco.constraint_schema
      |     and kcu.constraint_name = tco.constraint_name
      |where tco.constraint_type = 'PRIMARY KEY'
      |order by kcu.table_schema,
      |         kcu.table_name,
      |         kcu.ordinal_position )
      |""".stripMargin

  )

  val colSchemaName: Column[STRING, _ <: DatasetReadable[_]] = column[STRING]( "schema_name")
  val colTableName: Column[STRING, _ <: DatasetReadable[_]] = column[STRING]( "table_name")
  val colColumnName: Column[STRING, _ <: DatasetReadable[_]] = column[STRING]( "column_name")
  val colColumnTypeName: Column[STRING, _ <: DatasetReadable[_]] = column[STRING]( "constraint_type")

}

object PostgresColumnConstraintInterface extends MetaTableColumnConstraintsInterface {

  import org.mavok.api.Implicits._
  override val colTablePath: AbstractColumn[STRING,_ <: DatasetReadable[_]] = columnRef( PostgresTableColumnConstraints.colSchemaName concat "." concat ( PostgresTableColumnConstraints.colTableName ) )
  override val colColumnName: AbstractColumn[STRING,_ <: DatasetReadable[_]] = columnRef( PostgresTableColumnConstraints.colColumnName )
  override val colColumnConstraintType: AbstractColumn[STRING,_ <: DatasetReadable[_]]= columnRef( PostgresTableColumnConstraints.colColumnTypeName )
  override def tableQuery: QueryFlow[_] = DMLBuilder( PostgresTableColumnConstraints ).select( PostgresColumnConstraintInterface.getColumns: _* )


}

private object OracleTableColumnConstraints extends AbstractTable{



  // todo eventually this will get replaced with a proper implemention. Pending on the case statements
  override val tablePath: HasTablePath = TablePath(
    """
      |( SELECT schema_name, table_name, column_name, constraint_type
      |from (
      |         SELECT cols.owner schema_name,
      |                cols.table_name,
      |                cols.column_name,
      |                case
      |                    when cons.constraint_type = 'P' then 'PRIMARY_KEY'
      |                    when cons.constraint_type = 'U' then 'UNIQUE_KEY'
      |                    else 'Illegal case, should not be possible to reach here'
      |                    end    constraint_type
      |         FROM all_constraints cons,
      |              all_cons_columns cols
      |         WHERE cons.constraint_name = cols.constraint_name
      |           AND cons.owner = cols.owner
      |           and cons.constraint_type IN ('P', 'U')
      |         union all
      |         select owner      schema_name,
      |                table_name,
      |                column_name,
      |                'NOT_NULL' constraint_type
      |         from all_tab_columns
      |         where nullable = 'N'
      |     ) src
      |order by schema_name, table_name, column_name, constraint_type
      | )
      |""".stripMargin


  )

  val colSchemaName: Column[STRING, _ <: DatasetReadable[_]] = column[STRING]( "SCHEMA_NAME")
  val colTableName: Column[STRING, _ <: DatasetReadable[_]] = column[STRING]( "TABLE_NAME")
  val colColumnName: Column[STRING, _ <: DatasetReadable[_]] = column[STRING]( "COLUMN_NAME")
  val colColumnTypeName: Column[STRING, _ <: DatasetReadable[_]] = column[STRING]( "CONSTRAINT_TYPE")


}

object OracleTableColumnConstraintsInterface extends MetaTableColumnConstraintsInterface {

  import org.mavok.api.Implicits._
  override val colTablePath: AbstractColumn[STRING,_ <: DatasetReadable[_]] = columnRef( OracleTableColumnConstraints.colSchemaName concat "." concat ( PostgresTableColumnConstraints.colTableName ) )
  override val colColumnName: AbstractColumn[STRING,_ <: DatasetReadable[_]] = columnRef( OracleTableColumnConstraints.colColumnName )
  override val colColumnConstraintType: AbstractColumn[STRING,_ <: DatasetReadable[_]]= columnRef( OracleTableColumnConstraints.colColumnTypeName )
  override def tableQuery: QueryFlow[_] = DMLBuilder( OracleTableColumnConstraints ).select( OracleTableColumnConstraintsInterface.getColumns: _* )

}


object  MetaTableColumnContraints extends ContractList[MetaTableColumnConstraintsInterface] {
  override val defaultSchematic: MetaTableColumnConstraintsInterface = PostgresColumnConstraintInterface
  override val schematicOracle12: MetaTableColumnConstraintsInterface = OracleTableColumnConstraintsInterface
  override val schematicPostgres10: MetaTableColumnConstraintsInterface = PostgresColumnConstraintInterface

  def apply( _jdbcPlugin: JDBCPlugin): ReadableTableController[CaseClassMetaTableColumnConstraints, MetaTableColumnConstraintsInterface] = {


    val _tableQuery = getSchematicContract( _jdbcPlugin.getConnectionType )

    new ReadableTableController[CaseClassMetaTableColumnConstraints, MetaTableColumnConstraintsInterface](){
      override val jdbcPlugin: JDBCPlugin = _jdbcPlugin
      override val table: MetaTableColumnConstraintsInterface = _tableQuery
    }
  }

}





