package org.mavok.resource

import scala.collection.mutable.ListBuffer

/*
 * Copyright: blacktest -- created 05.06.19
 * Documentation: https://devhat.atlassian.net/wiki/spaces/ADM/overview
 */


trait Resource {


  def has(resource: Resource ): Boolean

}
