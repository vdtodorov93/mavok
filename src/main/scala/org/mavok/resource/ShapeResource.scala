package org.mavok.resource

import org.mavok.LOG

/*
 * Copyright: blacktest -- created 05.06.19
 * Documentation: https://devhat.atlassian.net/wiki/spaces/ADM/overview
 */


trait ShapeResource extends Resource {

  def has(resource: Resource ): Boolean = {
    resource equals this
  }
}
