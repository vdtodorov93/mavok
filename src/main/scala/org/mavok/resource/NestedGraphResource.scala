package org.mavok.resource

import org.mavok.LOG

import scala.collection.mutable.ListBuffer

/*
 * Copyright: blacktest -- created 05.06.19
 * Documentation: https://devhat.atlassian.net/wiki/spaces/ADM/overview
 */


object NestedGraphResource {
  def apply(): NestedGraphResource = new NestedGraphResource()
}


class NestedGraphResource private () extends Resource {

  private val listResources: ListBuffer[(Resource, Boolean)] = ListBuffer[(Resource, Boolean)]()

  def addResource(resource: Resource): Unit = {
    if( !listResources.exists( _._1 == resource ) ) {
      listResources += ( resource -> false )
    }

  }


  def getAll[T]( filterLocked: Boolean = false, depth: Int = 234567890 )(implicit m: Manifest[T]): Seq[T] = {
    val listAll = ListBuffer[T]()
    listResources.foreach {
      node =>

        val resource = node._1
        resource match {
          case element: T =>
              if( filterLocked == node._2 ){
                listAll += element.asInstanceOf[T]
              }
            else { listAll +=  element.asInstanceOf[T] }
          case _ => {}
        }

        if( resource.isInstanceOf[HasGraphResourceManager]
            && depth > 0
        ){
            listAll ++= resource.asInstanceOf[HasGraphResourceManager].graphResourceManager.getAll[T]( filterLocked, depth - 1  )
        }
    }

    listAll.toSeq
  }




  override def has(resource: Resource): Boolean = {

    if( resource.equals( this ) ) true else {
      val found = listResources.filter( _._1.has( resource ) )

      if( found.nonEmpty ) {
        true
      } else if (found.distinct.length != found.length) {
          LOG.error("Search Results", s"Duplicate results found for ${resource.toString}: found the following : ${found.mkString("  :  ")}",
            () => throw new IllegalArgumentException("Not allowed to be greater than 1 result or container with result"))
          false
      } else {
        false
      }
    }
  }

  def lock(resource: Resource): Unit = {

    listResources.foreach( _._1 match {
      case graph: NestedGraphResource => graph.lock( resource )
      case _ => {} // do nothing
    })

  }
}
