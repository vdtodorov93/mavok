package org.mavok.common.classvalue

/*
 * Copyright: blacktest -- created 09.05.19
 * Documentation: https://devhat.atlassian.net/wiki/spaces/ADM/overview
 */

case class User(user: String ) extends AnyVal
case class Password(password: String ) extends AnyVal
case class URL(URL: String ) extends AnyVal
case class Driver(driver: String ) extends AnyVal

case class SQL( value: String ) extends AnyVal



case class NameTable(table: String ) extends AnyVal
case class NameSchema(schema: String ) extends AnyVal
case class NameDatabase(database: String ) extends AnyVal
