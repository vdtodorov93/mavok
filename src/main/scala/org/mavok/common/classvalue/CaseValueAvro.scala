package org.mavok.common.classvalue

/*
 * Copyright: blacktest -- created 10.05.19
 * Documentation: https://devhat.atlassian.net/wiki/spaces/ADM/overview
 */

case class AvroName( name: String ) extends AnyVal
case class FilePath(path: String ) extends AnyVal

case class AvroTopic( name: String ) extends AnyVal
case class StringAvroDefinition(schema: String ) extends AnyVal