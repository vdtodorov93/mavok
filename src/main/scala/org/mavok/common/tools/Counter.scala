package org.mavok.common.tools

/*
 * Copyright: blacktest -- created 03.06.19
 * Documentation: https://devhat.atlassian.net/wiki/spaces/ADM/overview
 */




trait Counter {

    // ---------------------  Constructors
  private[this] var counter: Long = 0
  def next: Long = synchronized {
      counter += 1; counter
  }


  def current: Long = counter


}

class BasicCounter extends Counter {}

object BasicCounter {
  def apply(): BasicCounter = new BasicCounter()
}
