package org.mavok.common.tools

/*
 * Copyright: blacktest -- created 24.07.19
 * Documentation: https://devhat.atlassian.net/wiki/spaces/ADM/overview
 */



final class Memoize[A, R](f: A => R) extends (A => R) {

  // Cached function call results.
  private val result = scala.collection.mutable.Map.empty[A, R]

  def apply(a: A) = synchronized{
    val res = result.getOrElseUpdate(a, f(a))
    require( result.size == 1, s"Memoize received wrong input count ${result.size}. Trying to add ${a.toString} to results ${result.mkString}" )
    res
  }
  def get(): R = {
    require( result.nonEmpty, "Memoize not yet set")
    result.head._2
  }

  def getOption(): Option[R] = {
    if( result.isEmpty ) None else Some( result.head._2 )
  }
}

/** Memoization companion */
object Memoize {

  def apply[A,R](f:(A) => R ) = new Memoize[A,R](f)
}