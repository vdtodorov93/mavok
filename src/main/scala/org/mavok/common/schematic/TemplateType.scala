package org.mavok.common.schematic

/*
 * Copyright: blacktest -- created 11.05.19
 * Documentation: https://devhat.atlassian.net/wiki/spaces/ADM/overview
 */


object TemplateType extends Enumeration {

  val unknown, known = Value
}

