package org.mavok.common.schematic

import org.mavok.common.classvalue.SortedObject

import scala.collection.mutable.ListBuffer
import scala.reflect.runtime.universe.TypeTag


/*
 * Copyright: blacktest -- created 02.06.19
 * Documentation: https://devhat.atlassian.net/wiki/spaces/ADM/overview
 */


object SchematicBuilder {

  def apply[T](): SchematicBuilder[T] = new SchematicBuilder[T]()

}


class SchematicBuilder[T] {


  def add( element: T ): SchematicBuilder[T] = {
    elements += SortedObject( element, nextCounter )
    this
  }


  /**
    * Builds a new listBuffer of elements, sorted by SchematicTypeOrder, and then by Element Order
    * Schematics allow for create-if-not-exist with the provided lambda function, or a specific error to be thrown
    * Is not a deep-clone, as such a new instance will inherit any pointers provided
    * @param schematic
    * @return
    */
  def build( schematic: Schematic ): SchematicInstance  = {

    // create listbuffer of default generated objects. These get added only at conclusion of run
    val stagingCopyElements: ListBuffer[SortedObject[Any]] = elements.filter( _ => true )
    val sortedElementsUnknownAtBuild: ListBuffer[ ( SchematicTemplate[_], Seq[Any], Int  ) ] = ListBuffer[ ( SchematicTemplate[_], Seq[Any], Int ) ]()
    val sortedElementsKnownAtBuild: ListBuffer[ ( SchematicTemplate[_], Seq[Any], Int  ) ]= ListBuffer[ ( SchematicTemplate[_], Seq[Any], Int ) ]()
    // Check schematic for matches, try to autogen if missing.
    schematic.schematicList
      .filter( _.obj.templateType == TemplateType.unknown )
      .foreach {
      schematicContainer => {
        val schematicForm = schematicContainer.obj
        var matchingShapes = stagingCopyElements.filter(element => {
          //LoggerSchematic.debug( "Filter Condition for Schematic", s"${ element.obj.getClass  } == ${schematicForm.classType}")
          hasRelationship( element.obj, schematicForm.classType )
        })
        if (matchingShapes.isEmpty && !isValidCount( 0, schematicForm.count) ){

          if( isValidCount( 1, schematicForm.count) ){

            //LoggerSchematic.debug(s"Building Default for object ${schematicForm.classType}" , s"Current Elements: ${elements.map( element => element.obj.getClass ).mkString("  : ")}")


            val nextObject = SortedObject(askSchematicForDefault(schematicForm), nextCounter)
            // provide a chance for the default via callback to be generated. If that fails then we fail the build entirely
            stagingCopyElements += nextObject

            matchingShapes = stagingCopyElements.filter(element => {
              hasRelationship( element.obj, schematicForm.classType )
            } )
          } else {

            throw new IllegalStateException(s"Unable to generate default single value as schematic count ${schematicForm.count} does not allow ")

          }
        }
        if( ! isValidCount( matchingShapes.length, schematicForm.count ) ) {
          throw new IllegalArgumentException(
            s"Invalid count of values of ${schematicForm.toString}, count is ${matchingShapes.length}" +
              s". Schematic Count set to ${schematicForm.count}. Available forms ${elements.mkString("|")}"
          )
        }
        sortedElementsUnknownAtBuild += (( schematicForm, matchingShapes.sortBy{ _.sortOrder }.map( _.obj ), schematicContainer.sortOrder ))
      }
    }

    schematic.schematicList
      .filter( _.obj.templateType == TemplateType.known )
        .foreach{
          element => {
            sortedElementsKnownAtBuild += (( element.obj, Seq( askSchematicForDefault( element.obj ) ), element.sortOrder ))
          }
        }

    isInsideSchematicBuildScope( stagingCopyElements.map( _.obj ), schematic )

    val allRecords = sortedElementsUnknownAtBuild union sortedElementsKnownAtBuild sortBy( _._3 ) map ( element => ( element._1, element._2 ))

    SchematicInstance(  allRecords.toVector )
  }


  val elements: ListBuffer[SortedObject[Any]] = ListBuffer[SortedObject[Any]]()
  private var sortCounter = 0

  private def nextCounter: Int = { sortCounter += 1; sortCounter }

  private def isInsideSchematicBuildScope( elements: ListBuffer[Any], schematic: Schematic ): Boolean = {
    var isValid = true
    // if there do not exist
    elements.foreach(
      element => {


        // a shape which doesnt exist in the schematic
        if (!schematic.schematicList.exists(schematicTemplate => {
            val templateClass = schematicTemplate.obj.classType
            //LoggerSchematic.debug("Checking Dependencies",  s"${element} compared to ${templateClass}" )

            hasRelationship( element, templateClass )
          }
        )) {
          LoggerSchematic.error("Illegal Entity in Dependencies"
            , s"${element.toString} is not within Schematic. SchematicList: ${schematic.schematicList.map( _.obj.classType ).mkString(" : " )}"
            , () => throw new IllegalArgumentException(s"Any bound elements must be declared within Schematic. You can use the Schematic().withProp[YourType] method for this"))
          isValid = false
        }
      }

    )

    isValid
  }


  private def isValidCount[ClassTag: TypeTag]( foundCount: Int, schematicCount: Schematic.Value ): Boolean = {
    if (Schematic.isValidCount( foundCount, schematicCount )) {
      true
    } else {
        false
    }
  }


  private def askSchematicForDefault( schematic: SchematicTemplate[_] ): Any = {
    schematic.handleMissing()
  }




  private def hasRelationship( clazz1: Any, clazz2: Class[_] ): Boolean = {
    //LoggerSchematic.debug( "Comparing classes",  clazz1.getClass.getSimpleName + " ---- " + clazz2.getSimpleName )
    //LoggerSchematic.debug( "ResultComparison", s"${clazz1.getClass.isAssignableFrom( clazz2.getClass ) || clazz2.isAssignableFrom( clazz1.getClass )}")
    clazz1.getClass.isAssignableFrom( clazz2.getClass ) || clazz2.isAssignableFrom( clazz1.getClass )
  }

  /*
  private def getClassName( clazz: Class[_] ): String = {
    LoggerSchematic.debug( "Cleaning class string", clazz.getName )
    getCleanedName( clazz.getName )
  }


  private def getCleanedName( className: String ): String = {

    val dollarindx = className.indexOf("$")
    className.substring( 0, if( dollarindx > 0 ) dollarindx else className.length  )

  }

   */



}
