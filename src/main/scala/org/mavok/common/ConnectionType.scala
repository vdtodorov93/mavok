package org.mavok.common

/*
 * Copyright: blacktest -- created 23.05.19
 * Documentation: https://devhat.atlassian.net/wiki/spaces/ADM/overview
 */


object ConnectionType extends Enumeration {

  val ORACLE, POSTGRES, CASSANDRA = Value

}

