package org.mavok.api.column

import org.mavok.api.table.{DatasetPersistent, DatasetReadable}
import org.mavok.common.classvalue.Name
import org.mavok.datatype.dbtype.{DataType, _}
import org.mavok.jdbc.JDBCPlugin

/*
 * Copyright: blacktest -- created 06.06.19
 * Documentation: https://devhat.atlassian.net/wiki/spaces/ADM/overview
 */

import scala.reflect.runtime.universe._


object ColumnFactory {


  def getDefaultDBType[T <: DataType]( implicit typeTag: TypeTag[T] ): DBColumnType[T] = {
    typeOf[T] match {
      case t if t =:= typeOf[BIGINT] => DBColumnType.BIGINT.asInstanceOf[DBColumnType[T]]
      case t if t =:= typeOf[DOUBLE] => DBColumnType.DOUBLE( 38, 7 ).asInstanceOf[DBColumnType[T]]
      case t if t =:= typeOf[STRING] => DBColumnType.VARCHAR(255).asInstanceOf[DBColumnType[T]]
      case t if t =:= typeOf[INTEGER] => DBColumnType.INTEGER.asInstanceOf[DBColumnType[T]]
      case t if t =:= typeOf[LONG] => DBColumnType.LONG.asInstanceOf[DBColumnType[T]]
      case t if t =:= typeOf[TIMESTAMP] => DBColumnType.TIMESTAMP.asInstanceOf[DBColumnType[T]]
      case t if t =:= typeOf[FLOAT] => DBColumnType.FLOAT(38, 7).asInstanceOf[DBColumnType[T]]
      case t if t =:= typeOf[NUMBER] => DBColumnType.NUMBER(38, 7).asInstanceOf[DBColumnType[T]]
      case t if t =:= typeOf[DATE] => DBColumnType.DATE.asInstanceOf[DBColumnType[T]]
      case t if t =:= typeOf[BOOLEAN] => DBColumnType.BOOLEAN.asInstanceOf[DBColumnType[T]]
      case _ => throw new IllegalArgumentException(s"Couldnt find value ${typeOf[T]}")
    }
  }

  def buildColumn[SourceTag <: DatasetReadable[_], T <: DataType](dbColumnType: DBColumnType[T], source: DatasetPersistent, name: Name): Column[T, SourceTag] = {

    val column = new Column[T, SourceTag](
      source
      , name
      , dbColumnType
    )

    column
  }

  def buildColumnValue[T <: DataType](dbColumnType: DBColumnType[T], value: String): ColumnValue[T] = {
    new ColumnValue[T](
      value
      , dbColumnType
    )
  }

  // todo, plugin not used yet but added to make sure the values are database consistent
  def buildDefaultValueForType[T <: DataType]( columnType: DBColumnType[T], jdbcPlugin: JDBCPlugin  ): ColumnValue[T] = {

    columnType.typePattern.primitiveType match {

      case PrimitiveType.BIGINT | PrimitiveType.INTEGER | PrimitiveType.DOUBLE | PrimitiveType.NUMBER | PrimitiveType.FLOAT
      => ColumnFactory.buildColumnValue[T](  columnType, "-9995544" )
      case PrimitiveType.STRING => ColumnFactory.buildColumnValue[T](  columnType, "'-9995544'" )
      case PrimitiveType.DATE | PrimitiveType.TIMESTAMP => ColumnFactory.buildColumnValue[T](  columnType, "to_date( '0001-01-01', 'yyyy-mm-dd' )" )
      case _ => throw new IllegalArgumentException( s"Not found: ${columnType}")

    }

  }


  def isValidTagTypeMatch[T <: DataType](dbColumType: DBColumnType[T])( implicit typeTag: TypeTag[T] ): Boolean = {
    typeOf[T] match {
      case t if t =:= typeOf[STRING] => dbColumType.typePattern.primitiveType == PrimitiveType.STRING
      case t if t =:= typeOf[BIGINT] => dbColumType.typePattern.primitiveType == PrimitiveType.BIGINT
      case t if t =:= typeOf[LONG] => dbColumType.typePattern.primitiveType == PrimitiveType.LONG
      case t if t =:= typeOf[FLOAT] => dbColumType.typePattern.primitiveType == PrimitiveType.FLOAT
      case t if t =:= typeOf[DATE] => dbColumType.typePattern.primitiveType == PrimitiveType.DATE
      case t if t =:= typeOf[TIMESTAMP] => dbColumType.typePattern.primitiveType == PrimitiveType.TIMESTAMP
      case t if t =:= typeOf[NUMBER] => dbColumType.typePattern.primitiveType == PrimitiveType.NUMBER


    }
  }
}