package org.mavok.api.column

import org.mavok.api.table.{DatasetReadable, QueryModelGeneric}
import org.mavok.datatype.dbtype.{DBColumnType, DataType}

/*
 * Copyright: blacktest -- created 01.06.19
 * Documentation: https://devhat.atlassian.net/wiki/spaces/ADM/overview
 */



abstract class AbstractColumnExtender[T <: DataType](
      val baseColumn: AbstractColumn[_ <: DataType, _ <: DatasetReadable[_]]
    , dbType: DBColumnType[T]  )
  extends AbstractColumn[T, QueryModelGeneric]( baseColumn.columnPointer, dbType  ) {

  override protected val hashCodeString: String = baseColumn.toString + dbType.toString

  def getSourceColumn: AbstractColumn[_, _] = {
    baseColumn match {
      case extender: AbstractColumnExtender[_] => extender.getSourceColumn
      case _ => baseColumn
    }
  }


}



