package org.mavok.api.column

import org.mavok.api.table.DatasetReadable
import org.mavok.datatype.dbtype.{DBColumnType, DataType, STRING}
import org.mavok.resource.HasGraphResourceManager
import org.mavok.shapeLibrary.rdms.column.ColumnGraph
import org.mavok.shapeLibrary.rdms.column.extendedtypes.{ColumnExtendCast, ColumnExtendCount, ColumnExtendMax, ColumnExtendMin}
import org.mavok.workflowEngine.{ColumnCompositeComparisonFlow, ColumnFlow}

/*
 * Copyright: blacktest -- created 01.06.19
 * Documentation: https://devhat.atlassian.net/wiki/spaces/ADM/overview
 */
abstract class AbstractColumn[+T <: DataType, +SourceTag <: DatasetReadable[_] ](val columnPointer: String, val dbType: DBColumnType[_ <: T]  )
  extends HasGraphResourceManager with ColumnInterface{



  override def :==( that: AbstractColumn[_ <: DataType, _ <: DatasetReadable[_]] ): ColumnCompositeComparisonFlow = {
    ColumnFlow( this  ).:==( that )
  }



  override def :!=( that: AbstractColumn[_ <: DataType, _ <: DatasetReadable[_]] ): ColumnCompositeComparisonFlow = {
    ColumnFlow( this  ).:!=( that )
  }


  override def :>( that: AbstractColumn[_ <: DataType, _ <: DatasetReadable[_]] ): ColumnCompositeComparisonFlow = {
    ColumnFlow( this  ).:>( that )
  }

  override def :<( that: AbstractColumn[_ <: DataType, _ <: DatasetReadable[_]] ): ColumnCompositeComparisonFlow = {
    ColumnFlow( this  ).:<( that )
  }



  override def :<=( that: AbstractColumn[_ <: DataType, _ <: DatasetReadable[_]] ): ColumnCompositeComparisonFlow = {
    ColumnFlow( this  ).:<=( that )
  }

  override def :>=( that: AbstractColumn[_ <: DataType, _ <: DatasetReadable[_]] ): ColumnCompositeComparisonFlow = {
    ColumnFlow( this  ).:>=( that )
  }



  override def isNull: ColumnCompositeComparisonFlow = {
    ColumnFlow( this  ).isNull
  }

  override def isLike( that: AbstractColumn[ _ <: DataType, _ <: DatasetReadable[_]] ) : ColumnCompositeComparisonFlow = {
    ColumnFlow( this  ).isLike( that )
  }

  override def isNotNull: ColumnCompositeComparisonFlow = {
    ColumnFlow( this  ).isNotNull
  }




  override def isNotLike( that: AbstractColumn[ _ <: DataType, _ <: DatasetReadable[_]] ) : ColumnCompositeComparisonFlow = {
    ColumnFlow( this  ).isNotLike( that )
  }


  def getOrElse[ S >: T <: DataType] ( that: AbstractColumn[S, _ <: DatasetReadable[_]] ) : ColumnExtendGetOrElse[S] = {
    ColumnFlow( this  ).getOrElse( that )
  }


  override def :==( that: String ): ColumnCompositeComparisonFlow = {
    ColumnFlow( this  ).:==( that  )
  }



  override def :!=( that: String ): ColumnCompositeComparisonFlow = {
    ColumnFlow( this  ).:!=( that )
  }


  override def as[S <: DataType]( dbColumnType: DBColumnType[S]): ColumnExtendCast[S] = {
    ColumnFlow( this  ).as[S]( dbColumnType)
  }


  override def concat(that: AbstractColumn[_ <: DataType, _ <: DatasetReadable[_]]*): ColumnExtendConcat[STRING]
  = ColumnFlow( this  ).concat( that:_* )

  override def count: ColumnExtendCount = ColumnFlow( this  ).count

  override def min: ColumnExtendMin = ColumnFlow( this  ).min

  override def max: ColumnExtendMax = ColumnFlow( this  ).max


  def spawnReferenceShape: ColumnGraph


  override def equals( that: Any): Boolean = {
    if( that == null ) false
    else {
      this.hashCode == that.hashCode()
    }
  }

  override def hashCode: Int = hashCodeString.hashCode()


  // todo keep an eye on this, as its not really safe. Allows columns to '==' when they're not, but is required currently
  // to allow columns to 'understand' when they're being compared to the same column just in a different subquery
  protected val hashCodeString: String

}

