package org.mavok.api.column

import org.mavok.api.table.DatasetReadable
import org.mavok.datatype.dbtype.{DBColumnType, DataType}
import org.mavok.shapeLibrary.rdms.column.ColumnGraph
import org.mavok.shapeLibrary.rdms.column.extendedtypes.ColumnExtendCoalesceShape

/*
 * Copyright: blacktest -- created 24.08.19
 * Documentation: https://devhat.atlassian.net/wiki/spaces/ADM/overview
 */


class ColumnExtendGetOrElse[T <: DataType ]( leftBaseColumn: AbstractColumn[T, _ <: DatasetReadable[_]]
                                    , val rightBaseColumn: AbstractColumn[T, _ <: DatasetReadable[_]] )
  extends AbstractColumnExtender[T](  leftBaseColumn, leftBaseColumn.dbType.asInstanceOf[DBColumnType[T]]  ){

  override def spawnReferenceShape: ColumnGraph = {
    new ColumnExtendCoalesceShape( this )
  }
}



