package org.mavok.api.column

import org.mavok.api.table.DatasetReadable
import org.mavok.datatype.dbtype.{DBColumnType, DataType, STRING}
import org.mavok.shapeLibrary.rdms.column.extendedtypes.{ColumnExtendCast, ColumnExtendCount, ColumnExtendMax, ColumnExtendMin}
import org.mavok.workflowEngine.ColumnCompositeComparisonFlow
/*
 * Copyright: blacktest -- created 29.06.19
 * Documentation: https://devhat.atlassian.net/wiki/spaces/ADM/overview
 */
trait ColumnInterface {


  def :==( that: AbstractColumn[_ <: DataType, _ <: DatasetReadable[_]] ): ColumnCompositeComparisonFlow


  def :!=( that: AbstractColumn[_ <: DataType, _ <: DatasetReadable[_]] ): ColumnCompositeComparisonFlow


  def :>( that: AbstractColumn[_ <: DataType, _ <: DatasetReadable[_]] ): ColumnCompositeComparisonFlow

  def :<( that: AbstractColumn[_ <: DataType, _ <: DatasetReadable[_]] ): ColumnCompositeComparisonFlow



  def :<=( that: AbstractColumn[_ <: DataType, _ <: DatasetReadable[_]] ): ColumnCompositeComparisonFlow

  def :>=( that: AbstractColumn[_ <: DataType, _ <: DatasetReadable[_]] ): ColumnCompositeComparisonFlow

  def isNull: ColumnCompositeComparisonFlow


  def :==( that: String ): ColumnCompositeComparisonFlow


  def :!=( that: String ): ColumnCompositeComparisonFlow

  def isLike( that: AbstractColumn[_ <: DataType, _ <: DatasetReadable[_]] ) : ColumnCompositeComparisonFlow


  def isNotNull: ColumnCompositeComparisonFlow


  def isNotLike( that: AbstractColumn[ _ <: DataType, _ <: DatasetReadable[_]] ) : ColumnCompositeComparisonFlow

  def as[S <: DataType ](dbColumnType: DBColumnType[S] ): ColumnExtendCast[S]

  def concat( that: AbstractColumn[_ <: DataType, _ <: DatasetReadable[_]]* ): ColumnExtendConcat[STRING]

  def count: ColumnExtendCount

  def min: ColumnExtendMin

  def max: ColumnExtendMax

}
