package org.mavok.api.column

import org.mavok.api.table.QueryModelGeneric
import org.mavok.datatype.dbtype.{DBColumnType, DataType}
import org.mavok.shapeLibrary.rdms.column.ColumnGraph
import org.mavok.shapeLibrary.rdms.column.common.ColumnReference

/*
 * Copyright: blacktest -- created 01.06.19
 * Documentation: https://devhat.atlassian.net/wiki/spaces/ADM/overview
 */


protected object ColumnValue {


  def apply[T <: DataType](value: String, dbType: DBColumnType[T]  ): ColumnValue[T] = {
    new ColumnValue[T]( value, dbType )
  }

}


class ColumnValue[T <: DataType ](val value: String, dbType: DBColumnType[T]  )
  extends AbstractColumn[T, QueryModelGeneric ]( value, dbType  ) {


    private val self = this

    def spawnReferenceShape: ColumnGraph = new ColumnReference( this )

    protected val hashCodeString: String = value + dbType

  }
