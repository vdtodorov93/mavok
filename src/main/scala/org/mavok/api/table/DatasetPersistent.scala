package org.mavok.api.table

import org.mavok.api.column.Column
import org.mavok.datatype.dbtype.DataType
import org.mavok.shapeLibrary.shapeResource.DatasetContainer

import scala.collection.mutable.ListBuffer

/*
 * Copyright: blacktest -- created 23.08.19
 * Documentation: https://devhat.atlassian.net/wiki/spaces/ADM/overview
 */


trait DatasetPersistent extends DatasetReadable[Column[_, _]] {


  protected val listColumnHeaders = ListBuffer[Column[ _ <: DataType, _ <: this.type]]()

  protected lazy val staticColumnList = {
    listColumnHeaders.toVector
  }

  override def getColumns: Vector[Column[ _ <: DataType,  _ <: this.type]] = {
    require( staticColumnList.nonEmpty )
    staticColumnList
  }

  override lazy val asShape: DatasetContainer[Column[_, _]] = {
    val next = new DatasetContainer[Column[_, _]]( this  )
    staticColumnList.foreach( next.addOwnedResource )
    next
  }

  val tablePath: HasTablePath

}
