package org.mavok.api.table

import org.mavok.controller.TableControllerAvro
import org.mavok.datatype.Avro2DBColumnMapping
import org.mavok.jdbc.JDBCPlugin
/*
 * Copyright: blacktest -- created 23.08.19
 * Documentation: https://devhat.atlassian.net/wiki/spaces/ADM/overview
 */


class TableModelGeneric(_tablePath: TablePath, jdbcPlugin: JDBCPlugin) extends AbstractTable {

  override val tablePath: HasTablePath = _tablePath

  lazy val avroSchema: Avro2DBColumnMapping = Avro2DBColumnMapping.fromTable( this )
  lazy val controller: TableControllerAvro[this.type] = TableControllerAvro[this.type]( this, jdbcPlugin )

}


