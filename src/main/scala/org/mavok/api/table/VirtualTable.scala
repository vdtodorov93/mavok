package org.mavok.api.table

import org.mavok.LOG
import org.mavok.api.Implicits
import org.mavok.api.column.ColumnValue
import org.mavok.datatype.dbtype.{DBColumnType, DataType}
import org.mavok.shapeLibrary.shapeResource.DatasetContainer

import scala.collection.mutable.ListBuffer

/*
 * Copyright: blacktest -- created 23.08.19
 * Documentation: https://devhat.atlassian.net/wiki/spaces/ADM/overview
 */

@Deprecated
trait VirtualTable extends DatasetReadable[ColumnValue[_]] {

  protected val listColumnValues = ListBuffer[ColumnValue[ _ <: DataType ]]()

  override lazy val asShape: DatasetContainer[ColumnValue[_]] = { new DatasetContainer[ColumnValue[_]]( this  ) }

  override def getColumns: Vector[ColumnValue[ _ <: DataType ]] = {
    require( listColumnValues.nonEmpty )
    listColumnValues.toVector
  }

  protected def value[T <: DataType](value: String, dbType: DBColumnType[T] = null )( implicit m: Manifest[T] ): ColumnValue[T] = {
    // todo lots still remaining here to fix
    val column: ColumnValue[T] = Implicits.convertStringToColumn( value ).asInstanceOf[ColumnValue[T]]
    registerValue( column )
  }


  protected def registerValue[T <: DataType](column: ColumnValue[T]): ColumnValue[T] = {
    listColumnValues += column
    LOG.debug( s"Value added to table", s"${column.value}: ${column.dbType.toDDL}")
    column
  }
}

