package org.mavok.api.table

import org.mavok.controller.ReadableTableController
import org.mavok.jdbc.JDBCPlugin
import org.mavok.workflowEngine.QueryFlow

import scala.reflect.runtime.universe._
/*
 * Copyright: blacktest -- created 23.08.19
 * Documentation: https://devhat.atlassian.net/wiki/spaces/ADM/overview
 */


class QueryModel[TAG <: Product]( _tableQuery: QueryFlow[_], jdbcPlugin: JDBCPlugin)( implicit tag: TypeTag[TAG])
  extends QueryModelGeneric {

  override val tableQuery: QueryFlow[_] = _tableQuery

  lazy val controller: ReadableTableController[TAG, this.type] = ReadableTableController[TAG, this.type ]( this, jdbcPlugin )

}


