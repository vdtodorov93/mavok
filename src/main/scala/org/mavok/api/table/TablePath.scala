package org.mavok.api.table

import java.util.UUID
import java.util.UUID.randomUUID

/*
 * Copyright: blacktest -- created 17.08.19
 * Documentation: https://devhat.atlassian.net/wiki/spaces/ADM/overview
 */


object TablePath{


  def apply( tablePathSegments: String*  ): TablePath = new TablePath( tablePathSegments.mkString(".") ){}
  def apply( tablePath: String, UUID: UUID  ): TablePath = new TablePath( tablePath, UUID ){}

}



class TablePath(tablePointer: String, _uuid: UUID = randomUUID() ) extends HasTablePath {
  override def getTablePath: String = tablePointer
  override val uuid: UUID = _uuid
}

