package org.mavok.api.table

import java.util.UUID

/*
 * Copyright: blacktest -- created 11.08.19
 * Documentation: https://devhat.atlassian.net/wiki/spaces/ADM/overview
 */


trait HasTablePath {

  val uuid: UUID
  def getTablePath: String

}
