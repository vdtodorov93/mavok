package org.mavok.api.table

import org.mavok.controller.TableController
import org.mavok.jdbc.JDBCPlugin

import scala.reflect.runtime.universe._
/*
 * Copyright: blacktest -- created 23.08.19
 * Documentation: https://devhat.atlassian.net/wiki/spaces/ADM/overview
 */


class TableModel[TAG <: Product]( _tablePath: TablePath, jdbcPlugin: JDBCPlugin)( implicit tag: TypeTag[TAG]) extends AbstractTable {

  override val tablePath: HasTablePath = _tablePath

  lazy val controller: TableController[TAG, this.type] = TableController[TAG, this.type ]( this, jdbcPlugin )

}

object TableModel {

  def apply[TAG <: Product]( tablePath: TablePath, jdbcPlugin: JDBCPlugin)( implicit tag: TypeTag[TAG]): TableModel[TAG] = {
    new TableModel[TAG]( tablePath, jdbcPlugin )
  }

}
