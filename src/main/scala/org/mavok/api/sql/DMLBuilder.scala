package org.mavok.api.sql

import org.mavok.LOG
import org.mavok.api.column.{AbstractColumn, Column, ColumnValue}
import org.mavok.api.table.{DatasetReadable, VirtualTable}
import org.mavok.datatype.Avro2DBColumnMapping
import org.mavok.datatype.dbtype.DataType
import org.mavok.scope.Scope
import org.mavok.shapeLibrary.rdms.column.comparison.ComparisonGraph
import org.mavok.shapeLibrary.rdms.segment.Values
import org.mavok.workflowEngine.{InsertFlow, QueryFlow, UpdateFlow}

/*
 * Copyright: blacktest -- created 01.06.19
 * Documentation: https://devhat.atlassian.net/wiki/spaces/ADM/overview
 */


object DMLBuilder {

  // ---------------------  Constructors

  def apply[T <: DatasetReadable[_]](table: T ): DMLBuilder[T] = {
    new DMLBuilder( table )
  }

}

class DMLBuilder[T <: DatasetReadable[_]](table: T ){


  LOG.debug( "Query Started", "Parameters", table )




  def avroSchema: Avro2DBColumnMapping = null

  def inner( expressionColumnGraph: ComparisonGraph ): DMLBuilder[T] = {
    LOG.debug( "Inner Join Call", "Parameters", expressionColumnGraph )
    this
  }


  def merge( expressionColumnGraph: ComparisonGraph ): DMLBuilder[T] = {
    LOG.debug( "Merge Call", "Parameters", expressionColumnGraph )
    this
  }


  def apply( columns: AbstractColumn[_, T]* ): QueryFlow[T] = {
    select(columns: _* )
  }


  def select( columns: AbstractColumn[_, _]* ) : QueryFlow[T] = {
    val queryFLow: QueryFlow[T] = QueryFlow( table )
    queryFLow.select( columns: _* )
  }


  def insertValues( columns: ( Column[_, _], ColumnValue[_] )* ): InsertFlow[T] = {
    val values = Values( Scope(), columns.map( value => value._2 ))
    InsertFlow[T]( table, values ).insertBasic( columns.map( _._1 ): _*)
  }



  def updateValues( columns: ( Column[_, _], ColumnValue[_ <: DataType ] )* ): UpdateFlow[T] = {
    // todo remove this workaround


    val virtualTable = new VirtualTable(){
      columns.foreach(
        column =>
          registerValue( column._2 )
      )
    }

    QueryFlow( virtualTable )
      .select( columns.map( _._2 ):_* )
        .update( table )( columns.map( _._1 ): _* )
  }


  /*
    @deprecated
    def update[F, S <: ( BaseColumnHeader[_], BaseColumn[_])]( f: T => F )( implicit ev: F => DynamicTuple[S]): UpdateFlow[T] = {
      val columns: Vector[S] = ev( f( table  ) ).elements
      val query = QueryFlow[T]( table )
      query.dynamicSelect( columns.map( value => value._2 ) )
      UpdateFlow( query ).apply( columns.map( value  => value._1 ):_* )
    }*/


  /*
    def build( connectionType: ConnectionType.Value ): DBQuery = {
      query.defaultSchematic.preInitialize( table )

      val self = this
      new DBQuery {
        override val avroSchema: RecordSchema = self.avroSchema
        override val sql: SQL = query.toSQL( connectionType )
      }
    }*/

}
