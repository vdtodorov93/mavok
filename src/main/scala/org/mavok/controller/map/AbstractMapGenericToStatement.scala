package org.mavok.controller.map

import org.apache.avro.generic.GenericRecord
import org.mavok.api.table.TableModelGeneric
import org.mavok.datatype.dbtype.PrimitiveType

import scala.collection.mutable.ListBuffer

abstract class AbstractMapGenericToStatement( targetTable: TableModelGeneric ) {

  protected val template: Seq[String]
  protected val recordMarker = "*@:###RECORD_MARKER###*@:"



  def prepareStatement(_statement: GenericRecord  ): Seq[String] = {

    val orderedListValues: ListBuffer[( String, String )] = ListBuffer[( String, String )]()

    for( i <- targetTable.getColumns.indices ){

      val avroField = targetTable.avroSchema.fieldNames( i ).name
      val avroValue: String = _statement.get( avroField ).toString

      val nextValue = targetTable.avroSchema.fieldTypes( i ).typePattern.primitiveType match {
        case PrimitiveType.STRING => "'" + avroValue + "'"
        case _ => avroValue
      }

      val columnMarker = recordMarker + i
      val mappedValue =  ( columnMarker -> nextValue )
      orderedListValues += mappedValue
    }

    val statement: Vector[String] = template.toVector.map{
      segment => {
        if( segment.contains(recordMarker)){
          orderedListValues.filter( _._1 == segment ).head._2
        } else segment
      }
    }

    statement
  }


}
