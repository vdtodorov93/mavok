package org.mavok.controller.map

import org.mavok.api.sql.DMLBuilder
import org.mavok.api.table.{TableModelGeneric, VirtualTable}

class MapGenericToUpdateStatement( targetTable: TableModelGeneric )  extends AbstractMapGenericToStatement( targetTable ){

  import org.mavok.api.Implicits._
  override protected val template = setupUpdate
    .buildSQLBlocks( targetTable.controller.jdbcPlugin.getConnectionType ).map( _.value )


  private def setupUpdate = {

    val columns = { for( i <- targetTable.getColumns.indices ) yield {
      targetTable.getColumns(i) << convertStringToColumn( recordMarker + i, true )
    } }

    val virtualTable = new VirtualTable(){
      columns.foreach(
        column =>
          registerValue( column._2 )
      )
    }

    val pkColumns = columns.filter(
      sourceMap => targetTable.getColumns.filter( _.isPK )
        .exists( _.name.name == sourceMap._1.name.name)
    )

    require( pkColumns.nonEmpty, "Must have a PK column for an update statement" )
    require( pkColumns.length == targetTable.getColumns.count( _.isPK )
      , "Must include all PK columns in update statement" )

    val comparisons = pkColumns.map( mapping => mapping._1 :== mapping._2 )
    DMLBuilder( virtualTable )
      .select( columns.map( _._2 ):_* )
      .update( targetTable )( columns.map( _._1 ): _* )
      .on( comparisons.foldRight( comparisons.head )(  ( left, right ) => left && right ) )
  }

}

object MapGenericToUpdateStatement {

  def apply(targetTable: TableModelGeneric): MapGenericToUpdateStatement = new MapGenericToUpdateStatement(targetTable)

}



