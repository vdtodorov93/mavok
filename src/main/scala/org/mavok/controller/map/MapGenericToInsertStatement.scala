package org.mavok.controller.map

import org.mavok.api.sql.DMLBuilder
import org.mavok.api.table.TableModelGeneric

class MapGenericToInsertStatement( targetTable: TableModelGeneric ) extends AbstractMapGenericToStatement( targetTable ) {

  import org.mavok.api.Implicits._
  override protected val template: Seq[String] = DMLBuilder( targetTable )
    .insertValues(
      { for( i <- targetTable.getColumns.indices ) yield {
        targetTable.getColumns(i) << convertStringToColumn( recordMarker + i, true )
      } }.toSeq:_*)
    .buildSQLBlocks( targetTable.controller.jdbcPlugin.getConnectionType ).map( _.value )

}


object MapGenericToInsertStatement {

  def apply( table: TableModelGeneric ): MapGenericToInsertStatement = new MapGenericToInsertStatement( table )
}

