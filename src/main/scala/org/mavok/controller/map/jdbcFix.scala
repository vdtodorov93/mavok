package org.mavok.controller.map

import java.sql.ResultSet

import org.mavok.api.column.AbstractColumn
import org.mavok.api.table.DatasetReadable


/**
  * Addes fixes for several quicks of jdbc
  */
abstract class JDBCOverlay[T]( resultSet: ResultSet, table: DatasetReadable[AbstractColumn[_, _]]  ) {

  private val colResultIndexAdjustor = 1
  private var canIncrementResultSet = true

  validateResultSetMatchesTableSchema()

  def getJDBCIndex( index: Int ): Int = index + colResultIndexAdjustor


  def hasNext: Boolean = synchronized {
    if( canIncrementResultSet ){
      canIncrementResultSet = false
      resultSet.next()
    } else true // if canIncrementResultSet = false, this indicates the pointer was incremented - but it hasn't been read yet
  }

  def getNext(): T = {

    if( canIncrementResultSet ) hasNext

    val next = convertResultRecord()

    canIncrementResultSet = true
    next
  }

  def close(): Unit = {
    resultSet.close()
  }


  protected def convertResultRecord(): T


  private def validateResultSetMatchesTableSchema(): Unit = {
    // ------------------ Validate the query against expected results
    val columnCount = resultSet.getMetaData.getColumnCount
    if (columnCount != table.getColumns.length) {
      throw new IllegalAccessError(s"Actual column count $columnCount != expected ${table.getColumns.length}")
    }

  }

}
