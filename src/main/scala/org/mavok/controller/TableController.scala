package org.mavok.controller

import org.mavok.api.Implicits
import org.mavok.api.column.{Column, ColumnOption}
import org.mavok.api.sql.DMLBuilder
import org.mavok.api.table.{AbstractTable, VirtualTable}
import org.mavok.datatype.dbtype.{DataType, PrimitiveType}
import org.mavok.jdbc.JDBCPlugin
import org.mavok.workflowEngine.ColumnCompositeComparisonFlow

import scala.collection.mutable
import scala.collection.mutable.ListBuffer


/*
 * Copyright: blacktest -- created 20.08.19
 * Documentation: https://devhat.atlassian.net/wiki/spaces/ADM/overview
 */





import scala.reflect.runtime.universe._

/* todo Maybe tighter restrictions than Product.
 * Meant to let you take in Case Class Tables
 */


abstract class TableController[ Rec <: Product, Tab <: AbstractTable](implicit tag: TypeTag[Rec] )
  extends ReadableTableController[ Rec, Tab ]
    with DDLController[Tab] {

  override val table: Tab

  private val self = this



  def +=( next: Rec ): Unit = {

    val listValues: ListBuffer[AnyRef] = ListBuffer[AnyRef]()

    next.getClass.getDeclaredFields foreach { f =>
      f.setAccessible(true)
      f.getType
      listValues += f.get( next )
    }

    val mappings = table.getColumns zip listValues.map( value => value.toString() )

    jdbcPlugin.execStatement(
      DMLBuilder( table )
      .insertValues(

         mappings.map(

          mapping => mapping._1.dbType.typePattern.primitiveType match {
            case PrimitiveType.STRING => mapping._1 << Implicits.convertStringToColumn(mapping._2)
            case _ => mapping._1 << Implicits.convertStringToColumn(mapping._2, true)
          }
          ):_* )
        .build( jdbcPlugin.getConnectionType)  )

  }


  def update( next: Rec ): Unit = {

    val values = next.productIterator.toList.map( value =>  value.toString)

    //todo placeholder until correct assignment is written
    val virtualTable =  new VirtualTable{
      values.foreach{
        valueString => value( valueString )
      }
    }


    jdbcPlugin.execStatement(
      DMLBuilder( virtualTable )
      .select( virtualTable.getColumns: _* )
      .update(table)( table.getColumns: _* )
      .on( getMatchOnPKStatement( next ))
      .build( jdbcPlugin.getConnectionType )
    )
  }


  def set(assign: Tab => Seq[(Column[_, _], String)] ): SetOn.type = ???



  def merge( next: Rec ): Unit = {

    val results = filter( table => getMatchOnPKStatement( next ) )

    if( results.nonEmpty ){
      update( next )
    }else {
      += ( next )
    }
  }


  def delete( next: Rec ): Unit = ???

  def deleteWhere(f: Tab => ColumnCompositeComparisonFlow ): Unit = ???



  object SetOn {

    def on( f: self.type => ColumnCompositeComparisonFlow ): Seq[Rec] = ???

  }



  private def getMatchOnPKStatement( next: Rec ): ColumnCompositeComparisonFlow = {
    val listColumns: mutable.ListBuffer[( Int, Column[ _ <:DataType, _ <:this.table.type]) ]
    =  mutable.ListBuffer[ ( Int, Column[_ <: DataType, _ <: this.table.type] )]()
    // get positioning of primary key columns
    for( i <- table.getColumns.indices ){
      if( table.getColumns( i ).getOptions( ColumnOption.PRIMARY_KEY ) ){
        val column: ( Int, Column[ _<: DataType, _ <: this.table.type] ) = ( i, table.getColumns( i ) )
        listColumns.append( column )
      }
    }

    require( listColumns.nonEmpty, s"No primary key defined for table ${table.tablePath.getTablePath}")

    val conditions = listColumns.map( mapper => ( mapper._2 :== next.productElement( mapper._1 ).toString ) ).toVector
    conditions.foldRight(conditions.head)( ( l, r ) => ( l && r ) )
  }

}

object TableController {


  def apply[ Rec <: Product, Tab <: AbstractTable](_table: Tab, _jdbcPlugin: JDBCPlugin)(implicit tag: TypeTag[Rec] ): TableController[Rec, Tab]
  = new TableController[Rec,Tab]()(tag) {
    override val jdbcPlugin: JDBCPlugin = _jdbcPlugin
    override val table: Tab = _table
  }

}
