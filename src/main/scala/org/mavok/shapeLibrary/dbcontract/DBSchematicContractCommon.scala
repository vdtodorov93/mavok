package org.mavok.shapeLibrary.dbcontract

import org.mavok.shapeLibrary.rdms.common.GraphDBSchematicShape

/*
 * Copyright: blacktest -- created 22.08.19
 * Documentation: https://devhat.atlassian.net/wiki/spaces/ADM/overview
 */


trait DBSchematicContractCommon extends DBSchematicContract {

  final override lazy val schematicOracle12: GraphDBSchematicShape = defaultSchematic
  final override lazy val schematicPostgres10: GraphDBSchematicShape = defaultSchematic
}
