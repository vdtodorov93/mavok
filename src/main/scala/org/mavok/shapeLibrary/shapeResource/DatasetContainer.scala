package org.mavok.shapeLibrary.shapeResource

import org.mavok.api.column.AbstractColumn
import org.mavok.api.table.{AbstractTable, DatasetReadable, QueryModelGeneric, VirtualTable}
import org.mavok.common.schematic.Schematic
import org.mavok.scope.Scope
import org.mavok.shapeLibrary.rdms.common.{GraphDBSchematicShape, Mapping, ScopeResourceManager}
import org.mavok.shapeLibrary.rdms.keywords.SQLTablePointer

/*
 * Copyright: blacktest -- created 05.07.19
 * Documentation: https://devhat.atlassian.net/wiki/spaces/ADM/overview
 */



class DatasetContainer[ +T <: AbstractColumn[_, _] ](val sourceObject: DatasetReadable[T] )
  extends ColumnContainerGraphShape {

  override val columns: Seq[AbstractColumn[_, _]] = sourceObject.getColumns

  override val graphScope: Scope = Scope()

  override val defaultSchematic: GraphDBSchematicShape = new GraphDBSchematicShape {


    protected override def initialize_1stBuilder(): Unit = {


    }

    protected override def bindInContext_2ndBuilder(params: ScopeResourceManager): Unit = {
      addShapeTo2ndBuilder__BuildFromContext(
          sourceObject match {
            case pointer: VirtualTable => ReferenceTableAnonymous()
            case table: AbstractTable => SQLTablePointer( table.tablePath.getTablePath )
            case query: QueryModelGeneric => query.tableQuery.assembleGraph() // todo this isnt done
          }
      )
    }

    override def schematic(): Schematic =
      Schematic()
        .withFuture[Mapping](Schematic.ONE, () => throw new IllegalStateException("No pointer was setup for this table")
      )

  }

}