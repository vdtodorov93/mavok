package org.mavok.shapeLibrary.shapeResource

import org.mavok.api.column.AbstractColumn
import org.mavok.shapeLibrary.rdms.common.Sourceable

/*
 * Copyright: blacktest -- created 05.07.19
 * Documentation: https://devhat.atlassian.net/wiki/spaces/ADM/overview
 */


abstract class ColumnContainerGraphShape extends  Sourceable {

  val columns: Seq[AbstractColumn[_, _]]

}
