package org.mavok.shapeLibrary.rdms.keywords

import org.mavok.common.ConnectionType
import org.mavok.common.classvalue.SQL
import org.mavok.shapeLibrary.rdms.common.{MapTrail, Mapping}

import scala.collection.mutable.ListBuffer
/*
 * Copyright: blacktest -- created 24.05.19
 * Documentation: https://devhat.atlassian.net/wiki/spaces/ADM/overview
 */





class Shard(sql: String  )
  extends Mapping{
  final override def map( input: ConnectionType.Value, mapTrail: MapTrail = MapTrail( ListBuffer[String](), 0 ) ): Seq[SQL] = Seq( SQL( sql ))

}





object SQLColumn{
  def apply( name: String, table: SQLTable ): SQLColumn = new SQLColumn(name , table )
}

class SQLColumn(val name: String, val table: SQLTable  ) extends Shard(  name )



object SQLColumnExpression{
  def apply( expression: String ): SQLColumnExpression = new SQLColumnExpression( expression  )
}

class SQLColumnExpression( val expression: String ) extends Shard( expression )






object SQLColumnAlias {
  def apply( aliasName: String,  column: SQLColumn ): SQLColumnAlias = new SQLColumnAlias( aliasName, column )
}

class SQLColumnAlias(val aliasName: String, val column: SQLColumn ) extends Shard(  aliasName  )






object SQLTable {

  def apply( table: String ): SQLTable = new SQLTable( table)
}

class SQLTable( val table: String  ) extends Shard(  table )



object SQLTableAlias {

  def apply( aliasName: String,  table: SQLTable ): SQLTableAlias = new SQLTableAlias( aliasName, table )
}

class SQLTableAlias(val aliasName: String, val table: SQLTable  ) extends Shard(  aliasName  )







object SQLSchema {

  def apply( name: String ): SQLSchema = new SQLSchema( name )

}

class SQLSchema(val name: String ) extends Shard( name )




object SQLTablePointer {

  def apply( tablePointer: String ): SQLTablePointer = new SQLTablePointer( tablePointer )

}

class SQLTablePointer(val tablePointer: String ) extends Shard( tablePointer )
