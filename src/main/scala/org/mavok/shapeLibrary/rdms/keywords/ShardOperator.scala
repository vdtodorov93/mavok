package org.mavok.shapeLibrary.rdms.keywords

/*
 * Copyright: blacktest -- created 24.05.19
 * Documentation: https://devhat.atlassian.net/wiki/spaces/ADM/overview
 */


class ShardOperator(sql: String  ) extends KeyWord( sql ) {}

object _equals extends ShardOperator( " = " )
object _notEquals extends ShardOperator( " != " )
object _less extends ShardOperator( " < " )
object _greater extends ShardOperator( " > " )
object _lessEquals extends ShardOperator( " <= " )
object _greaterEquals extends ShardOperator( " >= " )

object _like extends ShardOperator( " LIKE " )
object _notLike extends ShardOperator( " NOT LIKE " )


object _isNull extends ShardOperator( " IS NULL " )
object _isNotNull extends ShardOperator( " IS NOT NULL " )

object _and extends ShardOperator( "AND" )
object _or  extends ShardOperator( "OR" )