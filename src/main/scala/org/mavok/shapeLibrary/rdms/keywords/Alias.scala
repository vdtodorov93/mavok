package org.mavok.shapeLibrary.rdms.keywords

import org.mavok.common.tools.Counter

/*
 * Copyright: blacktest -- created 05.06.19
 * Documentation: https://devhat.atlassian.net/wiki/spaces/ADM/overview
 */


object AliasCounter extends Counter


case class Alias( alias: String = s"zzz${AliasCounter.next}") extends Shard( alias )
