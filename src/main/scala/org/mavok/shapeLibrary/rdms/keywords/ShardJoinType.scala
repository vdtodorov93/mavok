package org.mavok.shapeLibrary.rdms.keywords

/*
 * Copyright: blacktest -- created 24.05.19
 * Documentation: https://devhat.atlassian.net/wiki/spaces/ADM/overview
 */


class ShardJoinType(sql: String  ) extends KeyWord( sql ) {}

object LEFT_JOIN extends ShardJoinType( "LEFT OUTER")
object RIGHT_JOIN extends ShardJoinType( "RIGHT OUTER")
object INNER_JOIN extends ShardJoinType( "INNER")
object FULL_JOIN extends ShardJoinType( "FULL")
object ON extends KeyWord( "ON" )