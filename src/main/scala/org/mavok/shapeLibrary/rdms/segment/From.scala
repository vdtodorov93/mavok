package org.mavok.shapeLibrary.rdms.segment

import org.mavok.common.schematic.Schematic
import org.mavok.common.tools.Memoize
import org.mavok.scope.Scope
import org.mavok.shapeLibrary.dbcontract.DBSchematicContractCommon
import org.mavok.shapeLibrary.rdms.common.{GraphDBSchematicShape, ScopeResourceManager, ScopedGraph}
import org.mavok.shapeLibrary.rdms.dbobject.DBObject
import org.mavok.shapeLibrary.rdms.keywords.{Alias, FROM, SPACE}
import org.mavok.shapeLibrary.shapeResource.ColumnContainerGraphShape
/*
 * Copyright: blacktest -- created 24.05.19
 * Documentation: https://devhat.atlassian.net/wiki/spaces/ADM/overview
 */


abstract class From extends ScopedGraph with DBSchematicContractCommon {

  private val self = this

  protected val source: ColumnContainerGraphShape


  override val defaultSchematic: GraphDBSchematicShape = new GraphDBSchematicShape {

    val tableMem = Memoize( ( source: ColumnContainerGraphShape) => { source } )
    val dbObjectMem = Memoize( ( params: ( ColumnContainerGraphShape, Scope ) ) => { DBObject( params._1, params._2 ) } )

    override def schematic(): Schematic =
      Schematic()
        .withKnown[FROM.type]( Schematic.ONE,  {FROM} )
        .withKnown[SPACE.type]( Schematic.ONE, SPACE )
        .withFuture[DBObject]( Schematic.ONE, () => throw new IllegalArgumentException( "No source found for query"))
        .withKnown[SPACE.type]( Schematic.ONE, SPACE )
        .withFuture[Alias]( Schematic.ONE, () => throw new IllegalArgumentException( "From alias needs to be set by the query") )

    protected override def initialize_1stBuilder(): Unit = {
      val table = tableMem( source )

      table.columns.foreach( column => graphResourceManager.addResource( column ))
      addOwnedResource( table )

      table.columns.foreach{
        column => addOwnedResource( column )
      }
      dbObjectMem( table, graphScope )
      require( source.columns.nonEmpty, s"No columns found in table ${table.toString}" )
      addShapeTo1stBuilder__Initialize( dbObjectMem.get() )
    }


    protected override def bindInContext_2ndBuilder(params: ScopeResourceManager): Unit = {
      addShapeTo2ndBuilder__BuildFromContext( dbObjectMem.get(), params )
      addShapeTo2ndBuilder__BuildFromContext( params.getAlias( self ) )
    }

  }

}

object From {
  def apply( _graphScope: Scope, _source: ColumnContainerGraphShape ): From = new From{
    val graphScope = _graphScope
    protected val source: ColumnContainerGraphShape = _source
  }
}





