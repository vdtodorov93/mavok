package org.mavok.shapeLibrary.rdms.segment

import org.mavok.api.column.{AbstractColumn, ColumnValue}
import org.mavok.common.schematic.Schematic
import org.mavok.scope.Scope
import org.mavok.shapeLibrary.dbcontract.DBSchematicContractCommon
import org.mavok.shapeLibrary.rdms.column.common.{ColumnHeaderInitParams, ColumnReferenceHeader}
import org.mavok.shapeLibrary.rdms.common.{GraphDBSchematicShape, ScopeResourceManager}
import org.mavok.shapeLibrary.rdms.keywords.{LEFT_PAREN, RIGHT_PAREN, SPACE, VALUES}
import org.mavok.shapeLibrary.shapeResource.ColumnContainerGraphShape



/*
 * You deliver the impossible
 * Copyright: blacktest -- created 25.05.19
 * Documentation: https://devhat.atlassian.net/wiki/spaces/ADM/overview
 */


abstract class Values extends ColumnContainerGraphShape with DBSchematicContractCommon {


  private val self = this

  protected val insertColumns: Seq[ColumnValue[_]]

  override val columns: Seq[AbstractColumn[_, _]] = insertColumns


  override val defaultSchematic: GraphDBSchematicShape = new GraphDBSchematicShape {

    import Schematic._
    override def schematic(): Schematic =
    Schematic()
      .withKnown( ONE, VALUES )
      .withKnown( ONE, LEFT_PAREN )
      .withKnown( ONE, SPACE )
      .withFuture[ColumnReferenceHeader]( ONE_OR_MORE, () => throw new IllegalArgumentException( "Wrong number of headers found"))
      .withKnown( ONE, SPACE )
      .withKnown( ONE, RIGHT_PAREN )




    protected override def initialize_1stBuilder(): Unit = {

      for( i <- insertColumns.indices ){
        val column = insertColumns( i )
        val header = ColumnReferenceHeader( column, ColumnHeaderInitParams( addAlias = false, addComma = { i != 0 }))
        addShapeTo1stBuilder__Initialize( header )
        addShapeTo2ndBuilder__BuildFromContext( header, self  )
      }

    }

    protected override def bindInContext_2ndBuilder(params: ScopeResourceManager ): Unit = {}

  }



}


object Values{

  def apply( scope: Scope, _columns: Seq[ColumnValue[_]]  ): Values = new Values{
    override protected val insertColumns: Seq[ColumnValue[_]] = _columns
    override val graphScope: Scope = scope
  }
}


