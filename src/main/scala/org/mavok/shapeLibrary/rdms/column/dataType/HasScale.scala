package org.mavok.shapeLibrary.rdms.column.dataType

/*
 * Copyright: blacktest -- created 29.07.19
 * Documentation: https://devhat.atlassian.net/wiki/spaces/ADM/overview
 */


trait HasScale {

  val scale: Int

}
