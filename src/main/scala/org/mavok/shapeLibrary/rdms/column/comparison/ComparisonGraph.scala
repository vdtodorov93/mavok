package org.mavok.shapeLibrary.rdms.column.comparison

import org.mavok.common.schematic.Schematic
import org.mavok.shapeLibrary.rdms.column.ColumnGraph
import org.mavok.shapeLibrary.rdms.common.{GraphDBSchematicShape, ScopeResourceManager}
import org.mavok.shapeLibrary.rdms.keywords.{NEWLINE, ShardOperator}
/*
 * Copyright: blacktest -- created 24.05.19
 * Documentation: https://devhat.atlassian.net/wiki/spaces/ADM/overview
 */









class ComparisonGraph(lColumn: ColumnGraph, operator: ShardOperator, rColumn: ColumnGraph  )
  extends ColumnComparison {
  override val defaultSchematic: GraphDBSchematicShape = new GraphDBSchematicShape {



    val leftColumnMem = ColumnWrapperComparisonGraph.lapply( lColumn )
    val rightColumnMem = ColumnWrapperComparisonGraph.rapply( rColumn )

    protected override def initialize_1stBuilder(): Unit = {
      addShapeTo1stBuilder__Initialize( leftColumnMem )
      addShapeTo1stBuilder__Initialize( rightColumnMem )
    }

    protected override def bindInContext_2ndBuilder(params: ScopeResourceManager): Unit = {
      addShapeTo2ndBuilder__BuildFromContext( leftColumnMem, params )
      addShapeTo2ndBuilder__BuildFromContext( rightColumnMem, params )
    }


    import Schematic._
    override def schematic(): Schematic =
      Schematic()
        .withFuture[LeftColumnWrapperComparisonGraph]( ONE, () =>  throw new IllegalArgumentException( "Not allowed to add column shape here as it needs binding") )
        .withKnown( ONE, NEWLINE )
        .withKnown( ONE, operator )
        .withFuture[RightColumnWrapperComparisonGraph]( ONE, () =>  throw new IllegalArgumentException( "Not allowed to add column shape here as it needs binding") )


  }
}

object ComparisonGraph {

  def apply(lColumn: ColumnGraph, operator: ShardOperator, rColumn: ColumnGraph ): ComparisonGraph
  = new ComparisonGraph( lColumn, operator, rColumn )
}






