package org.mavok.shapeLibrary.rdms.column

import org.mavok.resource.HasGraphResourceManager
import org.mavok.shapeLibrary.dbcontract.DBSchematicContractCommon
import org.mavok.shapeLibrary.rdms.common.Graph

/*
 * Copyright: blacktest -- created 29.06.19
 * Documentation: https://devhat.atlassian.net/wiki/spaces/ADM/overview
 */


trait ColumnGraphDefault extends ColumnGraph with DBSchematicContractCommon {

}
