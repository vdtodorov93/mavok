package org.mavok.shapeLibrary.rdms.column.extendedtypes

import org.mavok.api.column.{AbstractColumn, AbstractColumnExtender, Column}
import org.mavok.api.table.DatasetReadable
import org.mavok.common.schematic.Schematic
import org.mavok.common.tools.Memoize
import org.mavok.datatype.dbtype.{DBColumnType, DataType, LONG}
import org.mavok.shapeLibrary.rdms.column.common.{ColumnHeaderInitParams, ColumnReferenceHeader}
import org.mavok.shapeLibrary.rdms.column.{ColumnGraph, ColumnGraphDefault, extendedtypes}
import org.mavok.shapeLibrary.rdms.common.{GraphDBSchematicShape, ScopeResourceManager}
import org.mavok.shapeLibrary.rdms.keywords._

/*
 * Copyright: blacktest -- created 01.06.19
 * Documentation: https://devhat.atlassian.net/wiki/spaces/ADM/overview
 */


class ColumnExtendMax(  baseColumn: AbstractColumn[_ <: DataType, _ <: DatasetReadable[_]]  )
  extends AbstractColumnExtender[LONG]( baseColumn, DBColumnType.LONG ){

  override def spawnReferenceShape: ColumnGraph = {
    extendedtypes.ColumnExtendMaxShape( this )
  }
}


object ColumnExtendMaxShape{
  def apply(  column: AbstractColumnExtender[_]): ColumnExtendMaxShape = { new ColumnExtendMaxShape(  column ) }
}


class ColumnExtendMaxShape( val column: AbstractColumnExtender[_] ) extends ColumnGraphDefault {



  override val defaultSchematic: GraphDBSchematicShape = new GraphDBSchematicShape {

    val columnReferenceHeader = Memoize( ( value: ColumnReferenceHeader ) => value)


    protected override def initialize_1stBuilder(): Unit = {


      var columnHeaderInitParams: ColumnHeaderInitParams = null

      column.baseColumn match {

        case columnHeader: Column[_, _] =>
          columnHeaderInitParams = ColumnHeaderInitParams( addAlias = false, addComma = false, addSourceAlias = true )
        case _ => columnHeaderInitParams = ColumnHeaderInitParams( addAlias = false, addComma = false, addSourceAlias = false )
      }

      columnReferenceHeader.apply( ColumnReferenceHeader( column.baseColumn, columnHeaderInitParams ) )

      addShapeTo1stBuilder__Initialize( columnReferenceHeader.get() )


    }

    protected override def bindInContext_2ndBuilder(params: ScopeResourceManager): Unit = {
      addShapeTo2ndBuilder__BuildFromContext( columnReferenceHeader.get() , params )
    }

    override def schematic(): Schematic =
      Schematic()
        .withKnown( Schematic.ONE, MAX )
        .withKnown( Schematic.ONE, LEFT_PAREN )
        .withKnown( Schematic.ONE, SPACE )
        .withFuture[ColumnReferenceHeader]( Schematic.ONE, () => throw new IllegalArgumentException( "Cant find column for SQL header"))
        .withKnown( Schematic.ONE, SPACE )
        .withKnown( Schematic.ONE, RIGHT_PAREN )
        .withKnown( Schematic.ONE, SPACE )
  }

}


