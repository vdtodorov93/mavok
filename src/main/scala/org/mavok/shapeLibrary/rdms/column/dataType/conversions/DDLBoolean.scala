package org.mavok.shapeLibrary.rdms.column.dataType.conversions

import org.mavok.common.schematic.Schematic
import org.mavok.shapeLibrary.rdms.column.dataType.AbstractDDLType
import org.mavok.shapeLibrary.rdms.common._
import org.mavok.shapeLibrary.rdms.keywords._

/*
 * Copyright: blacktest -- created 29.07.19
 * Documentation: https://devhat.atlassian.net/wiki/spaces/ADM/overview
 */


abstract class DDLBoolean extends AbstractDDLType  {


  override def width: Int = 1

  override val schematicOracle12: GraphDBSchematicShape = new GraphDBSchematicShape {

    import org.mavok.common.schematic.Schematic._
    override def schematic(): Schematic =
      Schematic()
        .withKnown( ONE, SQLColumnExpression( "NUMBER") )
        .withKnown( ONE, LEFT_PAREN )
        .withKnown( ONE, SPACE )
        .withKnown( ONE, SQLColumnExpression( width.toString )  )
        .withKnown( ONE, COMMA )
        .withKnown( ONE, SQLColumnExpression( "0" )  )
        .withKnown( ONE, SPACE )
        .withKnown( ONE, RIGHT_PAREN )

    protected override def initialize_1stBuilder(): Unit = {

    }

    protected override def bindInContext_2ndBuilder(params: ScopeResourceManager ): Unit = {}

  }


  override val schematicPostgres10: GraphDBSchematicShape = new GraphDBSchematicShape {

    import org.mavok.common.schematic.Schematic._
    override def schematic(): Schematic =
      Schematic()
        .withKnown( ONE, SQLColumnExpression( "BOOLEAN") )

    protected override def initialize_1stBuilder(): Unit = {}

    protected override def bindInContext_2ndBuilder(params: ScopeResourceManager ): Unit = {}

  }



  override val defaultSchematic: GraphDBSchematicShape = new GraphDBSchematicShape {

    import org.mavok.common.schematic.Schematic._
    override def schematic(): Schematic =
      Schematic()
        .withKnown( ONE, SQLColumnExpression( "BOOLEAN") )

    protected override def initialize_1stBuilder(): Unit = {

    }

    protected override def bindInContext_2ndBuilder(params: ScopeResourceManager ): Unit = {}

  }



}

object DDLBoolean {
  def apply(): DDLBoolean = new DDLBoolean(){}
}

