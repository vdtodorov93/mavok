package org.mavok.shapeLibrary.rdms.column.common

import org.mavok.LOG
import org.mavok.api.column.{AbstractColumn, AbstractColumnExtender, Column, ColumnValue}
import org.mavok.common.schematic.Schematic
import org.mavok.common.schematic.Schematic.ONE
import org.mavok.shapeLibrary.rdms.column.{ColumnGraph, ColumnGraphDefault, TablePointer}
import org.mavok.shapeLibrary.rdms.common.{GraphDBSchematicShape, ScopeResourceManager}
import org.mavok.shapeLibrary.rdms.keywords.{Alias, COMMA, NEWLINE, SPACE}

/*
 * Copyright: blacktest -- created 24.05.19
 * Documentation: https://devhat.atlassian.net/wiki/spaces/ADM/overview
 */



class ColumnHeaderInitParams( val addComma: Boolean = false, val addAlias: Boolean = false, val addSourceAlias: Boolean = true )

object ColumnHeaderInitParams {
  def apply( addComma: Boolean = false, addAlias: Boolean = false, addSourceAlias: Boolean = true ): ColumnHeaderInitParams
  = new ColumnHeaderInitParams( addComma, addAlias, addSourceAlias )
}



abstract class ColumnReferenceHeader private() extends ColumnGraphDefault {

  val column: AbstractColumn[_, _]
  protected val columnParams: ColumnHeaderInitParams

  override val defaultSchematic: GraphDBSchematicShape = new GraphDBSchematicShape {

    var addAlias = false

    var columnShape: ColumnGraph = _

    override def schematic(): Schematic = {
      Schematic()
        .withFuture[COMMA.type]( Schematic.ZERO_OR_ONE, () => throw new IllegalArgumentException( "Too many values for commas"))
        .withKnown( Schematic.ONE, SPACE )
        .withFuture[TablePointer]( Schematic.ZERO_OR_ONE, () => throw new IllegalArgumentException( "Cant find column for SQL header"))
        .withFuture[ColumnGraph]( Schematic.ONE, () => throw new IllegalArgumentException( "Cant find alias for SQL header. This cant be safely generated as you need to bind to calling class dependency list"))
        .withKnown( Schematic.ONE, SPACE )
        .withFuture[Alias]( Schematic.ZERO_OR_ONE, () => throw new IllegalArgumentException( "Error setting up alias. This cant be safely generated as you need to bind to calling class dependency list"))
        .withKnown( ONE, NEWLINE )

    }

    protected override def initialize_1stBuilder(): Unit = {
      require( column != null )
      columnShape = column.spawnReferenceShape
      addShapeTo1stBuilder__Initialize( columnShape )
    }

    protected override def bindInContext_2ndBuilder(params: ScopeResourceManager): Unit = {

      if( columnParams.addComma ) addShapeTo2ndBuilder__BuildFromContext( COMMA, params )


      var rootColumn: AbstractColumn[_, _] = null
      var aliasManager: ScopeResourceManager = null
      // todo type detection should not be placed here

      column match {
        case colHeader: Column[_, _] =>

          if( columnParams.addAlias ) addShapeTo2ndBuilder__BuildFromContext( params.getAlias(  column  ), params )
          // todo should be picked up as column trait
          rootColumn = column
          aliasManager = getARM( params, rootColumn )

          if( columnParams.addSourceAlias ){
            val tablePointer = TablePointer( colHeader )
            addShapeTo2ndBuilder__BuildFromContext( tablePointer, params )
          }


        case columnExtender: AbstractColumnExtender[_] => {
          rootColumn = column
          aliasManager = params
        }

        case columnValue: ColumnValue[_] => {
          rootColumn = column
          aliasManager = getARM( params, rootColumn ) // todo the getARM method also adds a column value as an anonymous column if it doesnt exist. Should not do this
          if( columnParams.addAlias ) addShapeTo2ndBuilder__BuildFromContext( params.getAlias(  column  ) )

        }

        case _ => throw new IllegalArgumentException( s"Not allowed to add reach here")

      }

      addShapeTo2ndBuilder__BuildFromContext( columnShape, aliasManager )

    }


    private def getARM(params: ScopeResourceManager, rootColumn: AbstractColumn[_, _] ): ScopeResourceManager = {

      val listArmSources = params.getChildAliasManagerWithResouce( rootColumn )
      val listGlobalSources = params.getGlobalScopeManagerWithResouce( rootColumn )


      if( listArmSources.nonEmpty){
        listArmSources.head
      }
      else if( listGlobalSources.nonEmpty ){
        listGlobalSources.head.getChildAliasManagerWithResouce( rootColumn ).head

      }
      // todo this can probably be done better. If the column is not found, and it is a value, treat it as an anonymous column and add here
      else if( rootColumn.isInstanceOf[ColumnValue[_]] ){
        params.graphResourceManager.addResource( rootColumn )
        params.addOwnedResource( rootColumn )
        params

      } else {
        LOG.error( "No ARM sources found",
          s"${this} ${columnShape} ${rootColumn.columnPointer} has no child or global arms in ${params}", () =>
          throw new IllegalArgumentException( s"${this} ${columnShape} ${rootColumn} has no child or global arms in ${params}" +
            s"\nGlobals: ${params.getListGlobalScopeManagers}" +
            s"\nChildren: ${params.getListChildAliasManagers}" +
            s"\nChildrenOwnedResources: ${params.getListChildAliasManagers.map( _.getOwnedResources.mkString("|"))}" +
            s"" ))
        null
      }
    }

  }

}

object ColumnReferenceHeader {

  def apply(_column: AbstractColumn[_, _], _columnParams: ColumnHeaderInitParams ): ColumnReferenceHeader = {
    new ColumnReferenceHeader(){
      override val column: AbstractColumn[_, _] = _column
      override protected val columnParams: ColumnHeaderInitParams = _columnParams
    }
  }
}





