package org.mavok.shapeLibrary.rdms.column.dataType

import org.mavok.scope.Scope
import org.mavok.shapeLibrary.dbcontract.DBSchematicContract
import org.mavok.shapeLibrary.rdms.common.ScopedGraph

/*
 * Copyright: blacktest -- created 29.07.19
 * Documentation: https://devhat.atlassian.net/wiki/spaces/ADM/overview
 */



trait AbstractDDLType extends ScopedGraph with DBSchematicContract with HasWidth {
  override val graphScope: Scope = Scope()
}
