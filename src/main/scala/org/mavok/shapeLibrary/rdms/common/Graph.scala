package org.mavok.shapeLibrary.rdms.common

/*
 * Copyright: blacktest -- created 03.06.19
 * Documentation: https://devhat.atlassian.net/wiki/spaces/ADM/overview
 */


import org.mavok.LOG
import org.mavok.common.ConnectionType
import org.mavok.common.classvalue.{SQL, SortedObjectList}
import org.mavok.resource.HasGraphResourceManager
import org.mavok.shapeLibrary.dbcontract.DBSchematicContract

import scala.collection.mutable.ListBuffer
import scala.util.{Failure, Success, Try}


// todo push the HasGraphResourceManager to a specific RDBMS graph
abstract class Graph extends Shape with DBSchematicContract with HasGraphResourceManager {


  private val shapes1stBuilder_Initialize: SortedObjectList[Graph ] = new SortedObjectList[Graph]()
  private val shapes2ndBuilder_BuildInContext: SortedObjectList[( Mapping, ScopeResourceManager )] = new SortedObjectList[( Mapping, ScopeResourceManager )]()




  /**
    *   Shapes are added to whats called the 'buildscope'. A shape's build scope allows it be referred to from other objects within the tree
    *   At this point, no builders have been called, no constructors setup etc. This will be done later once we start the main loop
    * @param graph
    */
  final def addShapeTo1stBuilder__Initialize(graph: Graph ): Unit = {
    require( graph != null )
    require( !shapes1stBuilder_Initialize.getSorted.contains( graph ), s"Found duplicate ${graph} in ${shapes1stBuilder_Initialize.getSorted}")
    shapes1stBuilder_Initialize.add( graph )
  }


  /**
    * In keeping with the design principle of 'assign first, build second', objects are passed an alias binding
    * This allows shapes to bind their own subshapes to a given scope.
    * Example usage
    *     SELECT cust.CUSTOMERS custAlias from CUSTOMERS cust
    *
    *     -> Our column mapping 'CUSTOMERS' needs two bindings. The 'cust' alias from Customers, and the new Alias mapping from the given query
    *     -> Our column knows to listen for a table alias binding, so it waits for the right scope to be passed in. We pass in our TABLE object,
    *     wrapped in the FROM clause, which has already assigned the 'cust' alias binding to the TABLE object
    *     Our column then appropriates this mapping from the FROM scope
    *
    * @param graph
    * @param runTimeParams
    */
    //todo merge this DBSchematic Shape. Auto map shapes to bind params
  final def addShapeTo2ndBuilder__BuildFromContext(graph: Mapping, runTimeParams: ScopeResourceManager = null ): Unit = {
    require( graph != null )
    require( !shapes2ndBuilder_BuildInContext.getSorted.exists( _._1 ==  graph ))
    shapes2ndBuilder_BuildInContext.add( graph -> runTimeParams )
  }



  final def toSQL( connectionType: ConnectionType.Value ): Seq[SQL] = {

    val schematic = getSchematicContract( connectionType )
    LOG.debug( "Starting map", s"Parsing ${this.toString}")
    run1stBuilder_Initialize( connectionType )
    run2ndBuilder_BuildInContext( connectionType )
    map( connectionType )
  }


  private def incrementMapTrail( mapTrail: MapTrail ): MapTrail = {
    val listPrefixMarkers = for( i <- 0 to mapTrail.depth ) yield "--"
    mapTrail.builder += s"${listPrefixMarkers.mkString} ${this.toString}"
    MapTrail( mapTrail.builder, mapTrail.depth + 1)
  }





  final override def map( connectionType: ConnectionType.Value, mapTrail: MapTrail = MapTrail( ListBuffer[String]("SQLBuilder"), 1 ) ): Seq[SQL] = {

    validateNoMissingInitRuntime()


    val outputSQLString = ListBuffer[SQL]()

    val incMapTrail = incrementMapTrail( mapTrail )
    val buildElements = getSchematicContract( connectionType )
    val next = Try( buildElements.build(getSchematicContract( connectionType ).schematic) )
    next match {
      case Success(value) =>
        value.flatten
          .foreach( node => node match {
            case node: Mapping => outputSQLString ++= { node.map(connectionType, incMapTrail ) }
            case _ => throw new IllegalArgumentException(s"Shouldnt be possible to reach here - found type ${node}")
          }
      )
      case Failure(exception) => {
        LOG.error( "Failed in sqlBuilder, dumping trail", incMapTrail.builder.mkString( "\n") )
        throw exception
      }
    }

    outputSQLString
  }





  private def generateMissingRuntime(): Unit = {
    shapes1stBuilder_Initialize.getSorted.foreach{
      child =>
        val childElem = child
        if( !shapes2ndBuilder_BuildInContext.get.exists( _._1 ==  childElem  )){
          childElem match {
            case graph: Graph => {}
            case mapping: Mapping => addShapeTo2ndBuilder__BuildFromContext( childElem )
            case _ => throw new IllegalArgumentException( "Should not be possible to get this point")
          }
        }
    }
  }


  private def validateNoMissingInitRuntime(): Unit = {
    shapes1stBuilder_Initialize.getSorted.foreach{
      child =>
        val childElem = child

        if( !shapes2ndBuilder_BuildInContext.get.exists( _._1 ==  childElem  )){
          childElem match {
            case graph: Graph => throw new IllegalArgumentException( s"No runtime call for graph ${graph} in ${this} found." +
              s" You need to call the buildShapeFuture() for this graph. I cant run this as I need to know what alias you may need to bind it to")
            case _ => {}
          }
        }
    }
  }




  private def run1stBuilder_Initialize( connectionType: ConnectionType.Value, mapTrail: MapTrail = MapTrail( ListBuffer[String]("InitializeBuilder"), 1 ) ): Unit = {

    //LOG.debug( "Initial builder track", s"Diving into ${this}")
    val incMapTrail = incrementMapTrail( mapTrail )
    val tryBuilder = Try(
      getSchematicContract(connectionType ).setupInitBuild()
    )
    tryBuilder match {
      case Success(value) => {}
      case Failure( exception ) => LOG.error( "Failed in InitializeBuilder, dumping trail", incMapTrail.builder.mkString( "\n") )
        throw exception
    }

    shapes1stBuilder_Initialize.getSorted.foreach{
      child => {
        // Setup bindings for the initial build
        child.run1stBuilder_Initialize( connectionType )
        // Child to graph resouce manager, this places wihin the full query scope
        graphResourceManager.addResource( child )
      }
    }


  }


  private def run2ndBuilder_BuildInContext( connectionType: ConnectionType.Value, mapTrail: MapTrail = MapTrail( ListBuffer[String]("RuntimeBuilder"), 1 ) ): Unit = {

    //LOG.debug( "Runtime builder track", s"Diving into ${this}")
    //LOG.debug( "Runtime builder track", s"Init Vars ---> ${this.shapes1stBuilder_Initialize.get.mkString( "|||" )}")
    //LOG.debug( "Runtime builder track", s"Runtime Vars ---> ${this.shapes2ndBuilder_BuildInContext.get.mkString( "|||" )}")

    val incMapTrail = incrementMapTrail( mapTrail )
    val tryBuilder = Try( {
      this.getSchematicContract( connectionType ).setupBind()
      generateMissingRuntime()
    })
    tryBuilder match {
      case Success(value) => {}
      case Failure( exception ) => LOG.error( "Failed in RuntimeBuilder, dumping trail", incMapTrail.builder.mkString( "\n") )
        throw exception
    }

    shapes2ndBuilder_BuildInContext.getSorted.foreach {
      child =>
        child._1 match {
          case childElem: Graph => {
            childElem.getSchematicContract( connectionType ).setBindingParams( child._2 )
            childElem.run2ndBuilder_BuildInContext(connectionType, incMapTrail)
          }
          case _ => {}
        }
        this.getSchematicContract(connectionType).addShapeToBuildQueue( child._1  )
    }


  }






}










