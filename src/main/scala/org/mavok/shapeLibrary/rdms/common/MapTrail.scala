package org.mavok.shapeLibrary.rdms.common

import scala.collection.mutable.ListBuffer

case class MapTrail( builder: ListBuffer[String], depth: Int )