package org.mavok.shapeLibrary.rdms.common

import org.mavok.LOG
import org.mavok.common.schematic.{Schematic, SchematicBuilder, SchematicInstance}

// todo GraphDBSchematicShape needs to be decoupled from AliasResourceManager - this can be renamed to RDBMSGraphDBSchmaticShape
abstract class GraphDBSchematicShape extends DBSchematicShape[ ScopeResourceManager, Schematic, SchematicInstance] {

  lazy private val builder: SchematicBuilder[Mapping] = SchematicBuilder()


  final private[common] def addShapeToBuildQueue(element: Mapping ): SchematicBuilder[Mapping] = {
    if( !builder.elements.exists( _.obj == element ) ) {
      builder.add(element)
    } else {
      LOG.debug( "Skipping element",s"Skipping adding $element to build queue as already exists")
      builder
    }
  }


  override final def build(params: Schematic): SchematicInstance = {
    builder.build( params )
  }




}
