package org.mavok.shapeLibrary.rdms.common

import org.mavok.shapeLibrary.dbcontract.DBSchematicContractCommon

/*
 * Copyright: blacktest -- created 12.07.19
 * Documentation: https://devhat.atlassian.net/wiki/spaces/ADM/overview
 */


trait Sourceable extends ScopedGraph with DBSchematicContractCommon {

}
