package org.mavok.shapeLibrary.rdms.common

/*
 * Copyright: blacktest -- created 03.06.19
 * Documentation: https://devhat.atlassian.net/wiki/spaces/ADM/overview
 */


import org.mavok.LOG
import org.mavok.resource.{HasGraphResourceManager, Resource}
import org.mavok.scope.Scope
import org.mavok.shapeLibrary.rdms.keywords.Alias
import org.mavok.shapeLibrary.rdms.properties.{HasChildAliasManagers, IsOwnerOfResource}

import scala.collection.mutable.ListBuffer

trait ScopeResourceManager extends HasGraphResourceManager with IsOwnerOfResource with HasChildAliasManagers {

  val graphScope: Scope

  final def getAlias( element: Resource ): Alias = {
    require( has( element ), s"No element found for alias. " + s"Searching for ${element}  inside ${this}")
    graphResourceManager.lock(element)
    val alias = graphScope.getAlias(this, element)
    alias

  }

  final def addGlobalScopeManager(scopeResourceManager: ScopeResourceManager ): Unit = {
    graphScope.addGlobalAliasResourceManager( scopeResourceManager )
  }


  final def getListGlobalScopeManagers: Seq[ScopeResourceManager] = {
    graphScope.getGlobalAliasResourceManager
  }


  final def getGlobalScopeManagerWithResouce(resource: Resource ): Seq[ScopeResourceManager] = {
    require( graphScope.getGlobalAliasResourceManager.filter( _.has( resource ) ).length < 2, s"Too many returns ${graphScope.getGlobalAliasResourceManager.filter( _.has( resource ) )}. " +
      s"Searching in ${resource} with global list inside ${this}" )
    graphScope.getGlobalAliasResourceManager.filter( _.has( resource ) )
  }



}










