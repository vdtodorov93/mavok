package org.mavok.shapeLibrary.rdms.dbobject

import org.mavok.common.schematic.Schematic
import org.mavok.shapeLibrary.rdms.column.ColumnGraphDefault
import org.mavok.shapeLibrary.rdms.common.{GraphDBSchematicShape, ScopeResourceManager}

/*
 * Copyright: blacktest -- created 14.06.19
 * Documentation: https://devhat.atlassian.net/wiki/spaces/ADM/overview
 */



class DBColumn extends ColumnGraphDefault {


  override val defaultSchematic: GraphDBSchematicShape = new GraphDBSchematicShape{
    protected override def initialize_1stBuilder(): Unit = {

    }

    protected override def bindInContext_2ndBuilder(params: ScopeResourceManager): Unit = {

    }

    override def schematic(): Schematic =
      Schematic()
  }
}
