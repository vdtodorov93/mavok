package org.mavok.shapeLibrary.rdms.ddl.alterTable

import org.mavok.api.table.DatasetPersistent
import org.mavok.common.schematic.Schematic
import org.mavok.common.schematic.Schematic._
import org.mavok.scope.Scope
import org.mavok.shapeLibrary.dbcontract.DBSchematicContractCommon
import org.mavok.shapeLibrary.rdms.common.{GraphDBSchematicShape, ScopeResourceManager, ScopedGraph}
import org.mavok.shapeLibrary.rdms.dbobject.DBObject
import org.mavok.shapeLibrary.rdms.ddl.SimpleColumnName
import org.mavok.shapeLibrary.rdms.keywords._


/*
 * Copyright: blacktest -- created 25.05.19
 * Documentation: https://devhat.atlassian.net/wiki/spaces/ADM/overview
 */


abstract class AlterTablePrimaryKeyCreate extends ScopedGraph with DBSchematicContractCommon {

  val table: DatasetPersistent

  private val self = this

  override val defaultSchematic: GraphDBSchematicShape = new GraphDBSchematicShape{

    override def schematic(): Schematic =
      Schematic()
      .withKnown( ONE, {ALTER})
      .withKnown( ONE, {SPACE})
      .withKnown( ONE, {TABLE})
      .withKnown( ONE, {SPACE})
      .withFuture[DBObject]( ONE, () => throw new IllegalArgumentException( "No DBObject found"))
      .withKnown( ONE, SPACE )
      .withKnown( ONE, ADD )
      .withKnown( ONE, SPACE )
      .withKnown( ONE, PRIMARYKEY)
      .withKnown( ONE, SPACE )
      .withKnown( ONE, {LEFT_PAREN})
      .withFuture[SimpleColumnName]( ONE_OR_MORE, () => throw new IllegalArgumentException( "No Columns found"))
      .withKnown( ONE, {RIGHT_PAREN})

    protected override def initialize_1stBuilder(): Unit = {
      val tableShape = table.asShape

      val tableRef = DBObject( tableShape, graphScope )
      addShapeTo1stBuilder__Initialize( tableRef )
      addShapeTo2ndBuilder__BuildFromContext( tableRef )



      for( i <- table.getColumns.indices){
        val column = table.getColumns( i )
        if( column.isPK ){
          val ctColumn = SimpleColumnName( column, if( i == 0 ) false else true)
          addShapeTo1stBuilder__Initialize(ctColumn)
          addShapeTo2ndBuilder__BuildFromContext(ctColumn, self )
        }
      }


    }

    protected override def bindInContext_2ndBuilder(params: ScopeResourceManager): Unit = {}
  }
}

object AlterTablePrimaryKeyCreate {

  def apply(_table: DatasetPersistent, _scope: Scope ): AlterTablePrimaryKeyCreate = new AlterTablePrimaryKeyCreate(){
    val table: DatasetPersistent = _table
    override val graphScope: Scope = _scope
  }
}




