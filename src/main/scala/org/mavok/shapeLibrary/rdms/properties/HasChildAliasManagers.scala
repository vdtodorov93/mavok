package org.mavok.shapeLibrary.rdms.properties

import org.mavok.resource.Resource
import org.mavok.shapeLibrary.rdms.common.ScopeResourceManager

import scala.collection.mutable.ListBuffer

/*
 * Copyright: blacktest -- created 14.08.19
 * Documentation: https://devhat.atlassian.net/wiki/spaces/ADM/overview
 */


trait HasChildAliasManagers {

  private val listCollection: ListBuffer[ScopeResourceManager] = ListBuffer[ScopeResourceManager]()

  final def getListChildAliasManagers: Seq[ScopeResourceManager] = listCollection.filter(_ => true )


  final def addChildAliasManager( aliasResourceManager: ScopeResourceManager ): Unit = {
    require( !listCollection.contains( aliasResourceManager ), "Why are you adding an alias manager 2x?")
    listCollection += aliasResourceManager
  }

  final def getChildAliasManagerWithResouce(resource: Resource ): Seq[ScopeResourceManager] = {
    require( listCollection.filter( _.has( resource ) ).length < 2, s"Too many returns ${listCollection.filter( _.has( resource ) )}. " +
      s"Searching in ${resource} with child list inside ${this}" )


    listCollection.filter( _.has( resource ) )
  }



}
