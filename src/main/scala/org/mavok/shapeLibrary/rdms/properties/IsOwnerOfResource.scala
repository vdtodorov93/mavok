package org.mavok.shapeLibrary.rdms.properties

import org.mavok.resource.Resource
import org.mavok.shapeLibrary.rdms.common.Mapping

import scala.collection.mutable.ListBuffer

/*
 * Copyright: blacktest -- created 14.08.19
 * Documentation: https://devhat.atlassian.net/wiki/spaces/ADM/overview
 */


trait IsOwnerOfResource {

  private val listOwnedShapes: ListBuffer[Resource] = ListBuffer[Resource]()

  def getOwnedResources: Seq[Resource] = listOwnedShapes.filter( _ => true )

  def hasOwnedResource( resource: Resource ): Boolean = listOwnedShapes.contains( resource )

  def addOwnedResource( resource: Resource ): Unit = {
    listOwnedShapes += resource
  }

}
