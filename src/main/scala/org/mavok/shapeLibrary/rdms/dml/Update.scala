package org.mavok.shapeLibrary.rdms.dml

import org.mavok.api.column.{AbstractColumn, Column}
import org.mavok.common.schematic.Schematic
import org.mavok.common.schematic.Schematic._
import org.mavok.scope.Scope
import org.mavok.shapeLibrary.dbcontract.DBSchematicContractCommon
import org.mavok.shapeLibrary.rdms.column.common.ColumnReferenceHeader
import org.mavok.shapeLibrary.rdms.common.{GraphDBSchematicShape, ScopeResourceManager, ScopedGraph}
import org.mavok.shapeLibrary.rdms.dbobject.DBObject
import org.mavok.shapeLibrary.rdms.dml.rdbms.SetColumnListToQuery
import org.mavok.shapeLibrary.rdms.keywords._
import org.mavok.shapeLibrary.shapeResource.ColumnContainerGraphShape
import org.mavok.workflowEngine.{ColumnCompositeComparisonFlow, QueryFlow}

import scala.collection.mutable.ListBuffer



/*
 * Copyright: blacktest -- created 25.05.19
 * Documentation: https://devhat.atlassian.net/wiki/spaces/ADM/overview
 */


case class UpdateInitParams( test: String )



abstract class Update extends ScopedGraph with DBSchematicContractCommon {

  private val self = this

  protected val source: QueryFlow[_]
  protected val onCondition: ColumnCompositeComparisonFlow
  val insertMaps: Seq[( Column[_, _], AbstractColumn[_, _])]


  override lazy val defaultSchematic: GraphDBSchematicShape = new GraphDBSchematicShape {


    val targetTable = extractTargetFromColumnList( insertMaps )
    val dbObject =  DBObject( targetTable, graphScope )
    val setColumnListToQuery = SetColumnListToQuery( source, insertMaps, graphScope, onCondition )
    val columnHeaders: ListBuffer[ColumnReferenceHeader] = ListBuffer[ColumnReferenceHeader]()

    protected override def initialize_1stBuilder(): Unit = {

      targetTable.columns.foreach(
        column => {
          targetTable.addOwnedResource( column )
          targetTable.graphResourceManager.addResource( column )
        }

      )

      addChildAliasManager( targetTable )
      addGlobalScopeManager( self )
      addShapeTo1stBuilder__Initialize( setColumnListToQuery )
      addShapeTo1stBuilder__Initialize( dbObject )


    }

    protected override def bindInContext_2ndBuilder(params: ScopeResourceManager): Unit = {
      addShapeTo2ndBuilder__BuildFromContext( dbObject, self  )
      addShapeTo2ndBuilder__BuildFromContext( setColumnListToQuery, self )
      addShapeTo2ndBuilder__BuildFromContext( getAlias( targetTable ) )
    }

    override def schematic(): Schematic =
      Schematic()
        .withKnown( ONE, UPDATE )
        .withKnown( ONE, SPACE )
        .withFuture[DBObject]( ONE, () => throw new IllegalArgumentException( "No table found!") )
        .withKnown( ONE, SPACE )
        .withFuture[Alias]( ONE, () => throw new IllegalArgumentException( "No alias found!") )
        .withKnown( ONE, NEWLINE )
        .withKnown( ONE, SPACE )
        .withFuture[SetColumnListToQuery]( ONE, () => throw new IllegalArgumentException( "No column assignment found for query"))
    }


    private def extractTargetFromColumnList( insertMaps: Seq[( Column[_, _], AbstractColumn[_, _])] ): ColumnContainerGraphShape = {
      val targetColumns = insertMaps.map( _._1 )
      val targetTableCandidates = targetColumns.map( _.source.asShape ).distinct
      require( targetTableCandidates.length == 1, s"Wrong number of tables found expected 1 - found ${targetTableCandidates.mkString("|")}" )
      targetTableCandidates.head
    }


}

object Update {

  def apply(  _source: QueryFlow[_], _onCondition: ColumnCompositeComparisonFlow, _insertMaps: Seq[(Column[_, _], AbstractColumn[_, _])] ): Update
  = new Update{
    override val graphScope: Scope = Scope()
    override protected val source: QueryFlow[_] = _source
    override protected val onCondition: ColumnCompositeComparisonFlow = _onCondition
    override val insertMaps: Seq[(Column[_, _], AbstractColumn[_, _])] = _insertMaps
  }

}






