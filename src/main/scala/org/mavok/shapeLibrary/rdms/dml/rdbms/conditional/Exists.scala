package org.mavok.shapeLibrary.rdms.dml.rdbms.conditional

import org.mavok.common.schematic.Schematic
import org.mavok.scope.Scope
import org.mavok.shapeLibrary.dbcontract.DBSchematicContractCommon
import org.mavok.shapeLibrary.rdms.common.{GraphDBSchematicShape, ScopeResourceManager, ScopedGraph}

/*
 * Copyright: blacktest -- created 18.07.19
 * Documentation: https://devhat.atlassian.net/wiki/spaces/ADM/overview
 */


object Exists {

  // ---------------------  Constructors

  def apply( scope: Scope ): Exists = {
    new Exists {
      override val graphScope: Scope = scope
    }
  }

}

abstract class Exists extends ScopedGraph with DBSchematicContractCommon {

  override val defaultSchematic: GraphDBSchematicShape = new GraphDBSchematicShape {
    protected override def initialize_1stBuilder(): Unit = ???

    protected override def bindInContext_2ndBuilder(params: ScopeResourceManager): Unit = ???

    override def schematic(): Schematic = ???
  }
}
