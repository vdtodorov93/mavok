package org.mavok.shapeLibrary.rdms.dml

import org.mavok.api.column.AbstractColumn
import org.mavok.common.schematic.Schematic
import org.mavok.scope.Scope
import org.mavok.shapeLibrary.dbcontract.DBSchematicContractCommon
import org.mavok.shapeLibrary.rdms.common.{GraphDBSchematicShape, ScopeResourceManager}
import org.mavok.shapeLibrary.rdms.keywords.VALUES
import org.mavok.shapeLibrary.rdms.segment.{From, Select, SelectParams}
import org.mavok.shapeLibrary.shapeResource.ColumnContainerGraphShape



/*
 * You deliver the impossible
 * Copyright: blacktest -- created 25.05.19
 * Documentation: https://devhat.atlassian.net/wiki/spaces/ADM/overview
 */


abstract class InsertQuery() extends ColumnContainerGraphShape with DBSchematicContractCommon {

  val self = this

  protected val source: ColumnContainerGraphShape


  override val defaultSchematic: GraphDBSchematicShape = new GraphDBSchematicShape {

    var from: From = _

    def filterColumns( columns: Seq[AbstractColumn[_, _]] ): Select = {
      val select = Select( graphScope, SelectParams( self, columns ) )
      addShapeTo1stBuilder__Initialize( select  )
      addShapeTo2ndBuilder__BuildFromContext( select, self )
      select
    }


    import org.mavok.common.schematic.Schematic._
    override def schematic(): Schematic =
    Schematic()
      .withFuture[VALUES.type]( ZERO_OR_ONE, () => throw new IllegalArgumentException( "Wrong number of Values Keyword found") )
      .withFuture[Query]( ZERO_OR_ONE, () => throw new IllegalArgumentException( "Wrong number of Queries found")  )

    protected override def initialize_1stBuilder(): Unit = {
      from = From( graphScope, source  )
      addShapeTo1stBuilder__Initialize( from  )
    }

    protected override def bindInContext_2ndBuilder(params: ScopeResourceManager): Unit = {
      addShapeTo2ndBuilder__BuildFromContext( from, self )
    }
  }


}

object InsertQuery {

  def apply(insertColumns: Seq[AbstractColumn[_, _]], _source: ColumnContainerGraphShape ): InsertQuery = new InsertQuery(){
    override protected val source: ColumnContainerGraphShape = _source
    override val columns: Seq[AbstractColumn[_, _]] = insertColumns
    override val graphScope: Scope = Scope()
  }
}




