package org.mavok.shapeLibrary.rdms.dml

import org.mavok.api.column.{AbstractColumn, AbstractColumnExtender, Column, ColumnValue}
import org.mavok.common.schematic.Schematic
import org.mavok.scope.Scope
import org.mavok.shapeLibrary.dbcontract.DBSchematicContractCommon
import org.mavok.shapeLibrary.rdms.column.comparison.ColumnComparison
import org.mavok.shapeLibrary.rdms.common.{GraphDBSchematicShape, ScopeResourceManager}
import org.mavok.shapeLibrary.rdms.properties.{HasChildAliasManagers, IsOwnerOfResource}
import org.mavok.shapeLibrary.rdms.segment._
import org.mavok.shapeLibrary.shapeResource.ColumnContainerGraphShape



/*
 * You deliver the impossible
 * Copyright: blacktest -- created 25.05.19
 * Documentation: https://devhat.atlassian.net/wiki/spaces/ADM/overview
 */




abstract class Query(
                      val selectColumns: Seq[AbstractColumn[_,_]]
                      , val listJoins: Seq[JoinSegment]
                      , val whereColumns: Option[ColumnComparison]
                      , val groupColumns: Option[Seq[AbstractColumn[_,_]]]
                      , val orderColumns: Option[Seq[AbstractColumn[_,_]]]
                    ) extends ColumnContainerGraphShape
  with DBSchematicContractCommon
  with HasChildAliasManagers
  with IsOwnerOfResource {

  val self = this

  val source: ColumnContainerGraphShape


  override val columns: Seq[AbstractColumn[_,_]] = selectColumns



  override val defaultSchematic: GraphDBSchematicShape = new GraphDBSchematicShape {

    var from: From = _
    var where: Where = _
    var select: Select = _
    var groupBy: Group = _
    var orderBy: Order = _

    @scala.annotation.tailrec
    def isValidSelectColumn(column: AbstractColumn[_,_] ): Boolean = {
      column  match {
        case baseColumn: Column[_,_] => baseColumn.source.asShape == source
        case extendColumn: AbstractColumnExtender[_] => isValidSelectColumn( extendColumn.getSourceColumn )
        case valueColumn: ColumnValue[_] => true
        case _ => throw new IllegalArgumentException(s"No match found: ${column}")
      }
    }



    import Schematic._
    override def schematic(): Schematic =
      Schematic()
        .withFuture[Select]( ONE, () =>  throw new IllegalArgumentException( "No Select found")  )
        .withFuture[From]( ONE, () => throw new IllegalArgumentException( "No from in builder setup") )
        .withFuture[JoinSegment]( ZERO_OR_MORE, () => throw new IllegalArgumentException( "Should be impossible to reach here"))
        .withFuture[Where]( ZERO_OR_ONE, () => throw new IllegalArgumentException( "Wrong number of Where Segments")  )
        .withFuture[Group]( ZERO_OR_ONE, () => throw new IllegalArgumentException( "Wrong number of GroupBy Segments")  )
        .withFuture[Order]( ZERO_OR_ONE, () => throw new IllegalArgumentException( "Wrong number of OrderBy Segments")  )

    protected override def initialize_1stBuilder(): Unit = {

      require( columns.nonEmpty, "No columns found in table declaration")
      select = Select( graphScope, SelectParams( self, columns )  )
      addShapeTo1stBuilder__Initialize( select )

      selectColumns.foreach{ isValidSelectColumn }

      from = From( graphScope, source )
      addChildAliasManager( from )
      addShapeTo1stBuilder__Initialize( from )

      if( listJoins.nonEmpty ){
        listJoins.foreach{
          join => {
            addChildAliasManager( join )
            addShapeTo1stBuilder__Initialize( join )
          }
        }
      }

      whereColumns match {
        case Some(value) => {where = Where( value ); addShapeTo1stBuilder__Initialize( where ) }
        case None => {}
      }

      groupColumns match {
        case Some(value) => {groupBy = Group( graphScope, value );addShapeTo1stBuilder__Initialize( groupBy )}
        case None => {}
      }

      orderColumns match {
        case Some(value) => { orderBy = Order( graphScope, value );addShapeTo1stBuilder__Initialize( orderBy ) }
        case None => {}
      }

    }


    protected override def bindInContext_2ndBuilder(params: ScopeResourceManager): Unit = {
      //todo this needs update

      val actualParams =  self

      addShapeTo2ndBuilder__BuildFromContext( from, actualParams )

      if( listJoins.nonEmpty ){
        listJoins.foreach{
          join => addShapeTo2ndBuilder__BuildFromContext( join, actualParams )
        }
      }


      whereColumns match {
        case Some(value) => { addShapeTo2ndBuilder__BuildFromContext( where, actualParams ) }
        case None => {}
      }

      groupColumns match {
        case Some(value) => { addShapeTo2ndBuilder__BuildFromContext( groupBy, actualParams ) }
        case None => {}
      }


      orderColumns match {
        case Some(value) => { addShapeTo2ndBuilder__BuildFromContext( orderBy, actualParams )}
        case None => {}
      }

      addShapeTo2ndBuilder__BuildFromContext( select, actualParams )
    }


  }



}

object Query {

  def apply(scope: Scope
            , _selectColumns: Seq[AbstractColumn[_,_]]
            , _listJoins: Seq[JoinSegment]
            , _whereColumns: Option[ColumnComparison]
            , _groupColumns: Option[Seq[AbstractColumn[_,_]]]
            , _orderColumns: Option[Seq[AbstractColumn[_,_]]]
            , _source: ColumnContainerGraphShape
           ): Query = new Query(
    _selectColumns,  _listJoins, whereColumns = _whereColumns, groupColumns = _groupColumns, orderColumns = _orderColumns ) {


    override val graphScope: Scope = scope
    override val source: ColumnContainerGraphShape = _source
  }

}


