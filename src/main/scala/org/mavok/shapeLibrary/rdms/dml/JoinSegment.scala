package org.mavok.shapeLibrary.rdms.dml

import org.mavok.api.column.AbstractColumn
import org.mavok.api.table.DatasetReadable
import org.mavok.common.schematic.Schematic
import org.mavok.scope.Scope
import org.mavok.shapeLibrary.dbcontract.DBSchematicContractCommon
import org.mavok.shapeLibrary.rdms.column.comparison.ComparisonGraph
import org.mavok.shapeLibrary.rdms.common.{GraphDBSchematicShape, ScopeResourceManager, ScopedGraph}
import org.mavok.shapeLibrary.rdms.keywords._
import org.mavok.shapeLibrary.shapeResource.DatasetContainer
import org.mavok.workflowEngine.ColumnCompositeComparisonFlow



/*
 * Copyright: blacktest -- created 25.05.19
 * Documentation: https://devhat.atlassian.net/wiki/spaces/ADM/overview
 */




abstract class JoinSegment extends ScopedGraph with DBSchematicContractCommon  {


  private val self = this
  protected val table: DatasetReadable[AbstractColumn[_, _]]
  protected val joinType: ShardJoinType
  protected val comparisonGraph: ColumnCompositeComparisonFlow

  override val defaultSchematic: GraphDBSchematicShape = new GraphDBSchematicShape {



    override protected def initialize_1stBuilder(): Unit = {
      addOwnedResource( table.asShape )

      table.getColumns.foreach{ column =>
        graphResourceManager.addResource( column )
        addOwnedResource( column  )
      }

      addShapeTo1stBuilder__Initialize( table.asShape )
      addShapeTo1stBuilder__Initialize( comparisonGraph.expressionColumnGraph )
    }


    override protected def bindInContext_2ndBuilder(params: ScopeResourceManager): Unit = {
      addShapeTo2ndBuilder__BuildFromContext( table.asShape  )
      addShapeTo2ndBuilder__BuildFromContext( comparisonGraph.expressionColumnGraph, params )
      addShapeTo2ndBuilder__BuildFromContext( params.getAlias( self  ) )
    }

    import Schematic._
    override def schematic(): Schematic =
      Schematic()
        .withKnown( ONE, SPACE )
        .withKnown( ONE, NEWLINE )
        .withKnown( ONE, joinType )
        .withKnown( ONE, SPACE )
        .withKnown( ONE, JOIN )
        .withKnown( ONE, SPACE )
        .withFuture[DatasetContainer[_]]( ONE, () => throw new IllegalArgumentException( "No table shape found") )
        .withKnown( ONE, SPACE )
        .withFuture[Alias]( ONE, () => throw new IllegalArgumentException( "Alias required"))
        .withKnown( ONE, SPACE )
        .withKnown( ONE, NEWLINE )
        .withKnown( ONE, ON )
        .withKnown( ONE, SPACE )
        .withFuture[ComparisonGraph]( ONE, () => throw new IllegalArgumentException( "No join condition found"))

  }
  }

object JoinSegment {

  def apply(source: DatasetReadable[AbstractColumn[_, _]], join: ShardJoinType, compareStatement: ColumnCompositeComparisonFlow, scope: Scope ): JoinSegment = new JoinSegment() {
    override protected val table: DatasetReadable[AbstractColumn[_, _]] = source
    override protected val joinType: ShardJoinType = join
    override protected val comparisonGraph: ColumnCompositeComparisonFlow = compareStatement
    override val graphScope: Scope = scope
  }

}


